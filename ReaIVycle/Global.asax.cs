﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;

namespace ReaIVycle
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoute(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        private void RegisterRoute(RouteCollection routes)
        {
            foreach (Dictionary<string, string> route in ReaIVycle.app.Route.routes)
            {
                string[] routeValues = new string[3];
                int currentIndex = 0;
                foreach (KeyValuePair<string, string> routeInfo in route)
                {
                    routeValues[currentIndex] = routeInfo.Value;
                    ++currentIndex;
                }

                routes.MapPageRoute(routeValues[0], routeValues[1], routeValues[2]);
            }
        }
    }
}