USE [master]
GO
CREATE DATABASE [RealCycle]
GO
USE [RealCycle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetailTransaction]    Script Date: 3/27/2019 2:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetailTransaction](
	[HeaderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NULL,
	[Price] [float] NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_detail_transaction] PRIMARY KEY CLUSTERED 
(
	[HeaderID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HeaderTransaction]    Script Date: 3/27/2019 2:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HeaderTransaction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Occurrence] [datetime] NULL,
	[UserID] [int] NULL,
	[Status] [varchar](50) NULL,
 CONSTRAINT [PK_header_transaction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 3/27/2019 2:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Name] [varchar](255) NULL,
	[Price] [float] NULL,
	[Stock] [int] NULL,
	[PicturePath] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 3/27/2019 2:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Birthday] [datetime] NULL,
	[Gender] [nchar](10) NULL,
	[Phone] [varchar](20) NULL,
	[Address] [varchar](max) NULL,
	[Role] [varchar](10) NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([ID], [Name]) VALUES (1, N'Bike')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (2, N'Clothing')
INSERT [dbo].[Category] ([ID], [Name]) VALUES (3, N'Accessories')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ID], [CategoryID], [Name], [Price], [Stock], [PicturePath], [Description]) VALUES (6, 1, N'Light Bike', 1000000, 10, N'Light Bike.jpg', N'Very light-weight bike. Portable and suitable for everyone.')
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([ID], [Name], [Password], [Email], [Birthday], [Gender], [Phone], [Address], [Role]) VALUES (1, N'Administrator', N'dSPGKr23Yoxana2Pl9jYxcBA7eNlNeUxqKN0i2yufgA=', N'admin@gmail.com', CAST(N'1999-01-01T00:00:00.000' AS DateTime), N'Male      ', N'089892135000', N'Indonesia Street', N'Admin')
INSERT [dbo].[User] ([ID], [Name], [Password], [Email], [Birthday], [Gender], [Phone], [Address], [Role]) VALUES (2, N'test1234', N'9cvDyorOrQcEbzbk9lLuTzXGDNC006QJCkCeYcaQvhU=', N'test1234@gmail.com', CAST(N'1999-10-02T00:00:00.000' AS DateTime), N'Female    ', N'089892135000', N'Jl.K.H Syahdan Gg.Keluarga No.36 Street', N'Member')
INSERT [dbo].[User] ([ID], [Name], [Password], [Email], [Birthday], [Gender], [Phone], [Address], [Role]) VALUES (4, N'Ivan Elv', N'MfvUDGUjDedIFYfP/Hr/dLr2FB1GsmOn4x9A1fE8u4Y=', N'ivan.elianto@binus.ac.id', CAST(N'1999-10-17T00:00:00.000' AS DateTime), N'Male      ', N'089892135000', N'Gg.Keluarga No.39B K.H Syahdan Street', N'Member')
SET IDENTITY_INSERT [dbo].[User] OFF
/****** Object:  Index [IX_FK_detail_transaction_product]    Script Date: 3/27/2019 2:09:55 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_detail_transaction_product] ON [dbo].[DetailTransaction]
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_header_transaction_user]    Script Date: 3/27/2019 2:09:55 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_header_transaction_user] ON [dbo].[HeaderTransaction]
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FK_product_category]    Script Date: 3/27/2019 2:09:55 PM ******/
CREATE NONCLUSTERED INDEX [IX_FK_product_category] ON [dbo].[Product]
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DetailTransaction]  WITH CHECK ADD  CONSTRAINT [FK_detail_transaction_header_transaction] FOREIGN KEY([HeaderID])
REFERENCES [dbo].[HeaderTransaction] ([ID])
GO
ALTER TABLE [dbo].[DetailTransaction] CHECK CONSTRAINT [FK_detail_transaction_header_transaction]
GO
ALTER TABLE [dbo].[DetailTransaction]  WITH CHECK ADD  CONSTRAINT [FK_detail_transaction_product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO
ALTER TABLE [dbo].[DetailTransaction] CHECK CONSTRAINT [FK_detail_transaction_product]
GO
ALTER TABLE [dbo].[HeaderTransaction]  WITH CHECK ADD  CONSTRAINT [FK_header_transaction_user] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[HeaderTransaction] CHECK CONSTRAINT [FK_header_transaction_user]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_product_category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_product_category]
GO
USE [master]
GO
ALTER DATABASE [RealCycle] SET  READ_WRITE 
GO
