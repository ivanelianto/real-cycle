﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app
{
    public static class Route
    {
        private const string PHYSICALFILE_DIR = "~/app/View/";

        public static List<Dictionary<string, string>> routes = new List<Dictionary<string, string>>();

        static Route()
        {
            routes.Add(defaultRoute);

            routes.Add(loginRoute);
            routes.Add(registerRoute);
            routes.Add(logoutRoute);
            routes.Add(profileRoute);
            routes.Add(changePasswordRoute);
            routes.Add(createProductRoute);
            routes.Add(adminProductDetailRoute);
            routes.Add(productRoute);
            routes.Add(productDetailRoute);
            routes.Add(adminUserRoute);
            routes.Add(cartRoute);
            routes.Add(transactionRoute);
            routes.Add(transactionDetailRoute);
            routes.Add(reportRoute);
        }

        public static Dictionary<string, string> defaultRoute = new Dictionary<string, string>()
        {
            {"routeName", "Default"},
            {"routeUrl", ""},
            {"physicalFile", PHYSICALFILE_DIR + "Home.aspx"}
        };

        public static Dictionary<string, string> loginRoute = new Dictionary<string, string>()
        {
            {"routeName", "LoginRoute"},
            {"routeUrl", "login"},
            {"physicalFile", PHYSICALFILE_DIR + "Auth/Login.aspx"}
        };

        public static Dictionary<string, string> registerRoute = new Dictionary<string, string>()
        {
            {"routeName", "RegisterRoute"},
            {"routeUrl", "register"},
            {"physicalFile", PHYSICALFILE_DIR + "Auth/Register.aspx"}
        };

        public static Dictionary<string, string> logoutRoute = new Dictionary<string, string>()
        {
            {"routeName", "LogoutRoute"},
            {"routeUrl", "logout"},
            {"physicalFile", PHYSICALFILE_DIR + "Auth/Logout.aspx"}
        };

        public static Dictionary<string, string> profileRoute = new Dictionary<string, string>()
        {
            {"routeName", "ProfileRoute"},
            {"routeUrl", "profile"},
            {"physicalFile", PHYSICALFILE_DIR + "Profile.aspx"}
        };

        public static Dictionary<string, string> changePasswordRoute = new Dictionary<string, string>()
        {
            {"routeName", "ChangePasswordRoute"},
            {"routeUrl", "profile/change-password"},
            {"physicalFile", PHYSICALFILE_DIR + "ChangePassword.aspx"}
        };

        public static Dictionary<string, string> adminProductRoute = new Dictionary<string, string>()
        {
            {"routeName", "AdminProductRoute"},
            {"routeUrl", "admin/manage-product"},
            {"physicalFile", PHYSICALFILE_DIR + "Admin/ManageProduct/Index.aspx"}
        };

        public static Dictionary<string, string> adminProductDetailRoute = new Dictionary<string, string>()
        {
            {"routeName", "AdminProductDetailRoute"},
            {"routeUrl", "products/{id}/edit"},
            {"physicalFile", PHYSICALFILE_DIR + "Admin/ManageProduct/Detail.aspx"}
        };

        public static Dictionary<string, string> createProductRoute = new Dictionary<string, string>()
        {
            {"routeName", "CreateProductRoute"},
            {"routeUrl", "admin/manage-product/create"},
            {"physicalFile", PHYSICALFILE_DIR + "Admin/ManageProduct/Create.aspx"}
        };

        public static Dictionary<string, string> productRoute = new Dictionary<string, string>()
        {
            {"routeName", "ProductRoute"},
            {"routeUrl", "products"},
            {"physicalFile", PHYSICALFILE_DIR + "ProductPage.aspx"}
        };

        public static Dictionary<string, string> productDetailRoute = new Dictionary<string, string>()
        {
            {"routeName", "ProductDetailRoute"},
            {"routeUrl", "products/{id}"},
            {"physicalFile", PHYSICALFILE_DIR + "ProductDetail.aspx"}
        };

        public static Dictionary<string, string> adminUserRoute = new Dictionary<string, string>()
        {
            {"routeName", "AdminUserRoute"},
            {"routeUrl", "admin/users"},
            {"physicalFile", PHYSICALFILE_DIR + "Admin/ManageUser/Index.aspx"}
        };

        public static Dictionary<string, string> cartRoute = new Dictionary<string, string>()
        {
            {"routeName", "CartRoute"},
            {"routeUrl", "cart"},
            {"physicalFile", PHYSICALFILE_DIR + "Cart.aspx"}
        };

        public static Dictionary<string, string> transactionRoute = new Dictionary<string, string>()
        {
            {"routeName", "TransactionRoute"},
            {"routeUrl", "transactions"},
            {"physicalFile", PHYSICALFILE_DIR + "Transaction.aspx"}
        };

        public static Dictionary<string, string> transactionDetailRoute = new Dictionary<string, string>()
        {
            {"routeName", "TransactionDetailRoute"},
            {"routeUrl", "transactions/{id}"},
            {"physicalFile", PHYSICALFILE_DIR + "TransactionDetail.aspx"}
        };

        public static Dictionary<string, string> reportRoute = new Dictionary<string, string>()
        {
            {"routeName", "ReportRoute"},
            {"routeUrl", "report"},
            {"physicalFile", PHYSICALFILE_DIR + "Report.aspx"}
        };
    }
}