﻿using System.Globalization;

namespace ReaIVycle.app.Model
{
    public partial class Product
    {
        private const string imagePath = "/public/images/";

        public string GetPicturePath()
        {
            if (this.PicturePath == null || this.PicturePath == "")
            {
                return imagePath + "no-image.png";
            }
            else
            {
                return imagePath + "uploads/" + this.PicturePath;
            }
        }

        public string GetCategoryIconPath()
        {
            if (this.Category.Name == "Bike")
                return imagePath + "category-icons/bike.svg";
            else if (this.Category.Name == "Clothing")
                return imagePath + "category-icons/shirt.svg";
            else if(this.Category.Name == "Accessories")
                return imagePath + "category-icons/helmet.svg";

            return "";
        }

        public string GetFormattedPrice()
        {
            return ((double)this.Price).ToString("C", new CultureInfo("id-ID"));
        }
    }
}