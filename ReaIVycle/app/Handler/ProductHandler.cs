﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System.Collections.Generic;

namespace ReaIVycle.app.Handler
{
    public class ProductHandler
    {
        public static List<Category> GetAllCategories()
        {
            return CategoryRepository.GetAllCategories();
        }

        public static List<Product> GetAllProducts()
        {
            return ProductRepository.GetAllProducts();
        }

        public static List<Product> FindByCategory(int categoryID)
        {
            return ProductRepository.FindByCategory(categoryID);
        }

        public static Product FindByID(int id)
        {
            return ProductRepository.FindByID(id);
        }

        public static void DeleteProduct(int productID)
        {
            ProductRepository.Delete(productID);
        }

        public static void AddProduct(Product product)
        {
            ProductRepository.Add(product);
        }

        public static void Update(Product product)
        {
            ProductRepository.Update(product);
        }
    }
}