﻿using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System.Collections.Generic;

namespace ReaIVycle.app.Handler
{
    public static class TransactionHandler
    {
        public static HeaderTransaction GetCurrentUserCart(User user)
        {
            return TransactionRepository.GetCurrentUserCartData(user);
        }

        public static void AddTransaction(HeaderTransaction header)
        {
            TransactionRepository.AddTransaction(header);
        }

        public static void UpdateTransaction(HeaderTransaction header)
        {
            TransactionRepository.UpdateTransactionDetail(header);
        }

        public static void UpdateTransaction(HeaderTransaction header, DetailTransaction detail)
        {
            TransactionRepository.UpdateTransactionDetail(header, detail);
        }

        public static void RemoveItem(int headerID, int productID)
        {
            TransactionRepository.RemoveItem(headerID, productID);
        }

        public static void Checkout(HeaderTransaction transaction)
        {
            TransactionRepository.Checkout(transaction);
        }

        public static List<HeaderTransaction> FindTransactionsByUser(User user)
        {
            return TransactionRepository.FindTransactionsByUser(user);
        }

        public static List<HeaderTransaction> GetTransactions()
        {
            return TransactionRepository.GetTransactions();
        }

        public static HeaderTransaction FindTransactionByID(int id)
        {
            return TransactionRepository.FindTransactionByID(id);
        }

        public static void ApproveTransaction(HeaderTransaction transaction)
        {
            TransactionRepository.ChangeTransactionStatusTo(transaction,
                TransactionFactory.APPROVED_STATUS);
        }
    }
}