﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System.Collections.Generic;

namespace ReaIVycle.app.Handler
{
    public class HomeHandler
    {
        public static List<Product> GetAllProduct()
        {
            return ProductRepository.GetAllProducts();
        }
    }
}