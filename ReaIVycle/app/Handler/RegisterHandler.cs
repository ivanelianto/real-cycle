﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Handler
{
    public class RegisterHandler
    {
        public static void Register(User user)
        {
            UserRepository.Register(user);
        }
    }
}