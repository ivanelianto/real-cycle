﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System.Collections.Generic;

namespace ReaIVycle.app.Handler
{
    public class UserHandler
    {
        public static List<User> GetAllUsers()
        {
            return UserRepository.GetAllUsers();
        }

        public static void Register(User user)
        {
            UserRepository.Register(user);
        }

        public static void Promote(int id)
        {
            UserRepository.Promote(id);
        }

        public static void UpdatePassword(User user)
        {
            UserRepository.UpdatePassword(user);
        }

        public static User FindUserByEmail(string email)
        {
            return UserRepository.FindByEmail(email);
        }

        public static void Delete(int id)
        {
            UserRepository.Delete(id);
        }

        public static User FindUserByID(int id)
        {
            return UserRepository.FindByID(id);
        }
    }
}