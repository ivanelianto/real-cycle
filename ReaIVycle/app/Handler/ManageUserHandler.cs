﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Handler
{
    public class ManageUserHandler
    {
        public static List<User> GetAllUsers()
        {
            return UserRepository.GetAllUsers();
        }

        public static void Promote(int id)
        {
            UserRepository.Promote(id);
        }

        public static void Delete(int id)
        {
            UserRepository.Delete(id);
        }
    }
}