﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Handler
{
    public class LoginHandler
    {
        public static User Login(string email, string password)
        {
            return UserRepository.Login(email, password);
        }
    }
}