﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Handler
{
    public class ManageProductHandler
    {
        public static List<Category> GetAllCategories()
        {
            return CategoryRepository.GetAllCategories();
        }

        public static Product GetProductByID(int id)
        {
            return ProductRepository.FindByID(id);
        }

        public static void Add(Product product)
        {
            ProductRepository.Add(product);
        }

        public static void Update(Product product)
        {
            ProductRepository.Update(product);
        }

        public static void Delete(int id)
        {
            ProductRepository.Delete(id);
        }
    }
}