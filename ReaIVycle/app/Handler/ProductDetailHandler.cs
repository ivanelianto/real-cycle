﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Handler
{
    public class ProductDetailHandler
    {
        public static Product GetProductByID(int id)
        {
            return ProductRepository.FindByID(id);
        }
    }
}