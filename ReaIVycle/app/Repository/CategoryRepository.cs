﻿using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Repository
{
    public class CategoryRepository
    {
        public static List<Category> GetAllCategories()
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                return db.Categories.ToList();
            }
        }
    }
}