﻿using ReaIVycle.app.Model;
using System.Collections.Generic;
using System.Linq;

namespace ReaIVycle.app.Repository
{
    public static class ProductRepository
    {
        public static List<Product> GetAllProducts()
        {
            List<Product> products = new List<Product>();

            using (RealCycleEntities db = new RealCycleEntities())
            {
                products = db.Products
                    .Include("Category")
                    .Select(x => x).ToList();
            }

            return products;
        }

        public static List<Product> FindByCategory(int categoryID)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                return db.Products
                    .Include("Category")
                    .Where(x => x.CategoryID == categoryID)
                    .Select(x => x)
                    .ToList();
            }
        }

        public static Product FindByID(int id)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                return db.Products
                    .Include("Category")
                    .Where(x => x.ID == id)
                    .FirstOrDefault();
            }
        }

        public static void Add(Product product)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                db.Products.Add(product);
                db.SaveChanges();
            }
        }

        public static void Update(Product product)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                Product _product = db.Products.Find(product.ID);
                _product.Name = product.Name;
                _product.CategoryID = product.CategoryID;
                _product.Price = product.Price;
                _product.Stock = product.Stock;
                _product.Description = product.Description;
                _product.PicturePath = product.PicturePath;
                db.SaveChanges();
            }
        }

        public static void Delete(int id)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                Product productToDelete = db.Products.Find(id);
                db.Products.Remove(productToDelete);
                db.SaveChanges();
            }
        }
    }
}