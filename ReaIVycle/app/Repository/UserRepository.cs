﻿using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using ReaIVycle.app.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace ReaIVycle.app.Repository
{
    public class UserRepository
    {
        public static User Login(string email, string password)
        {
            User user = null;

            using (RealCycleEntities db = new RealCycleEntities())
            {
                string hashedPassword = SecurityHelper.Hash(password);

                user = db.Users
                    .Where(x =>
                        x.Email == email &&
                        x.Password == hashedPassword)
                    .FirstOrDefault();
            }

            return user;
        }

        public static User Register(User user)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                user = db.Users.Add(user);
                db.SaveChanges();
            }

            return user;
        }

        public static void Promote(int id)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                db.Users.Find(id).Role = "Admin";
                db.SaveChanges();
            }
        }

        public static void UpdatePassword(User user)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                db.Users.Find(user.ID).Password = user.Password;
                db.SaveChanges();
            }
        }

        public static void Delete(int id)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                User user = db.Users.Find(id);
                db.Users.Remove(user);
                db.SaveChanges();
            }
        }

        public static User FindByID(int id)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                return db.Users
                    .Where(x => x.ID == id)
                    .FirstOrDefault();
            }
        }

        public static User FindByEmail(string email)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                return db.Users
                    .Where(x => x.Email == email)
                    .FirstOrDefault();
            }
        }

        public static List<User> GetAllUsers()
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                return db.Users.ToList();
            }
        }
    }
}