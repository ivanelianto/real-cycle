﻿using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using System.Collections.Generic;
using System.Linq;

namespace ReaIVycle.app.Repository
{
    public static class TransactionRepository
    {
        public static void AddTransaction(HeaderTransaction transaction)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                var header = db.HeaderTransactions.Add(transaction);
                db.SaveChanges();
            }
        }

        public static void UpdateTransactionDetail(HeaderTransaction transaction)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                AddTransactionHeader(transaction, db);

                foreach (DetailTransaction detail in transaction.DetailTransactions)
                {
                    db.DetailTransactions.Attach(detail);
                    db.Entry(detail).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public static void UpdateTransactionDetail(
            HeaderTransaction transaction,
            DetailTransaction detail)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                AddTransactionHeader(transaction, db);

                db.DetailTransactions.Add(detail);
                db.SaveChanges();
            }
        }

        public static void Checkout(HeaderTransaction transaction)
        {
            ChangeTransactionStatusTo(transaction, TransactionFactory.PENDING_STATUS);

            using (RealCycleEntities db = new RealCycleEntities())
            {
                foreach (DetailTransaction detail in transaction.DetailTransactions)
                {
                    Product product = db.Products.Find(detail.ProductID);
                    product.Stock -= detail.Quantity;

                    db.Products.Attach(product);
                    db.Entry(product).State = System.Data.Entity.EntityState.Modified;
                }

                db.SaveChanges();
            }
        }

        public static void ChangeTransactionStatusTo(HeaderTransaction transaction, string status)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                transaction.Occurrence = System.DateTime.Now;
                transaction.Status = status;

                db.HeaderTransactions.Attach(transaction);
                db.Entry(transaction).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
            }
        }

        private static void AddTransactionHeader(HeaderTransaction transaction, RealCycleEntities db)
        {
            transaction.Occurrence = System.DateTime.Now;
            db.HeaderTransactions.Attach(transaction);
            db.Entry(transaction).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public static HeaderTransaction GetCurrentUserCartData(User user)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                var result = db.HeaderTransactions
                    .Where(x => x.UserID == user.ID)
                    .Where(x => x.Status == TransactionFactory.IN_CART_STATUS)
                    .Select(x => x)
                    .FirstOrDefault();

                if (result != null)
                {
                    db.Entry(result).Collection("DetailTransactions").Load();

                    foreach (DetailTransaction detail in result.DetailTransactions)
                    {
                        db.Entry(detail).Reference("Product").Load();
                        db.Entry(detail.Product).Reference("Category").Load();
                    }
                }

                return result;
            }
        }

        public static void RemoveItem(int headerID, int productID)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                var detailToDelete = db.DetailTransactions.Find(headerID, productID);

                db.DetailTransactions.Remove(detailToDelete);

                db.SaveChanges();
            }
        }

        public static List<HeaderTransaction> FindTransactionsByUser(User user)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                return db.HeaderTransactions
                    .Include("User")
                    .Where(x => x.UserID == user.ID)
                    .Select(x => x)
                    .ToList();
            }
        }

        public static List<HeaderTransaction> GetTransactions()
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                var result = db.HeaderTransactions
                    .Include("DetailTransactions")
                    .Include("DetailTransactions.Product")
                    .Include("User")
                    .Select(x => x)
                    .ToList();

                return result;
            }
        }

        public static HeaderTransaction FindTransactionByID(int id)
        {
            using (RealCycleEntities db = new RealCycleEntities())
            {
                var result = db.HeaderTransactions
                    .Where(x => x.ID == id)
                    .Where(x => x.Status != TransactionFactory.IN_CART_STATUS)
                    .Select(x => x)
                    .FirstOrDefault();

                if (result != null)
                {
                    db.Entry(result).Reference("User").Load();
                    db.Entry(result).Collection("DetailTransactions").Load();

                    foreach (DetailTransaction detail in result.DetailTransactions)
                    {
                        db.Entry(detail).Reference("Product").Load();
                        db.Entry(detail.Product).Reference("Category").Load();
                    }
                }

                return result;
            }
        }
    }
}