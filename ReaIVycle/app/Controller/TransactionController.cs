﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Factory;
using ReaIVycle.app.Handler;
using ReaIVycle.app.Model;
using ReaIVycle.app.Util;
using ReaIVycle.app.Validator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReaIVycle.app.Controller
{
    public static class TransactionController
    {
        private static HeaderTransaction itemsInCart;

        public static HeaderTransaction GetCurrentUserCart()
        {
            var user = SessionManager.GetInstance().GetCurrentUser();

            return TransactionHandler.GetCurrentUserCart(user);
        }

        public static HeaderTransaction GetCurrentUserCart(User user)
        {
            return TransactionHandler.GetCurrentUserCart(user);
        }

        public static string IsValidAddToCartInput(int productID, AddToCartDTO dto)
        {
            AddToCartValidator validator = new AddToCartValidator(dto.Clone() as AddToCartDTO);

            var errors = validator.Validate();

            return errors.Count() > 0 ? errors.First() : IsExceedStock(productID, dto);
        }

        private static string IsExceedStock(int productID, AddToCartDTO dto)
        {
            var finalQuantity = dto.Quantity;

            var itemsInCart = GetCurrentUserCart();

            if (itemsInCart != null)
            {
                foreach (DetailTransaction detail in itemsInCart.DetailTransactions)
                    if (detail.ProductID == productID)
                        finalQuantity = (detail.Quantity + int.Parse(dto.Quantity)).ToString();
            }

            AddToCartValidator validator = new AddToCartValidator(new AddToCartDTO(dto.Quantity, finalQuantity));

            var errors = validator.Validate();

            return errors.Count() > 0 ? errors.First() : null;
        }

        public static void AddTransaction(Product product, string finalQuantity)
        {
            int quantity = int.Parse(finalQuantity);

            DetailTransactionDTO detailDTO = new DetailTransactionDTO()
            {
                Price = product.Price.Value,
                ProductID = product.ID,
                Quantity = quantity
            };

            DetailTransaction detailTransaction = TransactionFactory.CreateDetail(detailDTO);

            var user = SessionManager.GetInstance().GetCurrentUser();

            itemsInCart = TransactionHandler.GetCurrentUserCart(user);

            if (itemsInCart == null)
                AddFirstItemToCart(detailTransaction, user);
            else
                AddNewItemToCart(product, detailDTO, detailTransaction);
        }

        private static void AddNewItemToCart(
            Product product, 
            DetailTransactionDTO detailDTO, 
            DetailTransaction detailTransaction)
        {
            bool isInCart = false;

            foreach (DetailTransaction detail in itemsInCart.DetailTransactions)
            {
                if (detail.ProductID == product.ID)
                {
                    detail.Quantity += detailDTO.Quantity;
                    isInCart = true;
                    break;
                }
            }

            detailTransaction.HeaderID = itemsInCart.ID;

            if (!isInCart)
                TransactionHandler.UpdateTransaction(itemsInCart, detailTransaction);

            TransactionHandler.UpdateTransaction(itemsInCart);
        }

        private static void AddFirstItemToCart(DetailTransaction detailTransaction, User user)
        {
            HeaderTransactionDTO dto = new HeaderTransactionDTO()
            {
                Occurrence = DateTime.Now,
                Status = TransactionFactory.IN_CART_STATUS,
                User = user
            };

            itemsInCart = TransactionFactory.CreateHeader(dto);
            itemsInCart.DetailTransactions.Add(detailTransaction);
            TransactionHandler.AddTransaction(itemsInCart);
        }

        public static void RemoveItem(int headerID, int productID)
        {
            TransactionHandler.RemoveItem(headerID, productID);
        }

        public static void Checkout()
        {
            var header = GetCurrentUserCart();

            TransactionHandler.Checkout(header);
        }

        public static List<HeaderTransaction> FindTransactionsByUser(User user)
        {
            return TransactionHandler.FindTransactionsByUser(user);
        }

        public static List<HeaderTransaction> GetTransactions()
        {
            return TransactionHandler.GetTransactions();
        }

        public static HeaderTransaction FindTransactionByID(int id)
        {
            return TransactionHandler.FindTransactionByID(id);
        }

        public static void ApproveTransaction(HeaderTransaction transaction)
        {
            TransactionHandler.ApproveTransaction(transaction);
        }
    }
}