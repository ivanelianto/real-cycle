﻿using ReaIVycle.app.Handler;
using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Controller
{
    public static class CategoryController
    {
        public static List<Category> GetAllCategories()
        {
            return ProductHandler.GetAllCategories();
        }
    }
}