﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Handler;
using ReaIVycle.app.Model;
using ReaIVycle.app.Validator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Controller
{
    public static class ProductController
    {
        public static string IsValidInput(ProductDTO dto)
        {
            ProductValidator validator = new ProductValidator(dto);

            var error = validator.Validate();

            return error.Count() > 0 ? error.First() : null;
        }

        public static List<Product> GetAllProducts()
        {
            return ProductHandler.GetAllProducts();
        }

        public static List<Product> FindProductByCategory(int categoryID)
        {
            return ProductHandler.FindByCategory(categoryID);
        }

        public static Product FindProductByID(int id)
        {
            return ProductHandler.FindByID(id);
        }

        public static void DeleteProduct(string productID)
        {
            var id = int.Parse(productID);
            ProductHandler.DeleteProduct(id);
        }

        public static void AddProduct(Product product)
        {
            ProductHandler.AddProduct(product);
        }

        public static void Update(Product product)
        {
            ProductHandler.Update(product);
        }
    }
}