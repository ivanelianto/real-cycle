﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Factory;
using ReaIVycle.app.Handler;
using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using ReaIVycle.app.Util;
using ReaIVycle.app.Validator;
using System.Linq;

namespace ReaIVycle.app.Controller
{
    public static class AuthController
    {
        public static bool IsValidLoginInput(string email, string password)
        {
            return !string.IsNullOrEmpty(email) && 
                !string.IsNullOrEmpty(password);
        }

        public static string IsValidRegistrationInput(RegisterDTO dto)
        {
            RegistrationValidator validator = new RegistrationValidator(dto);

            var errors = validator.Validate();

            return errors.Count() > 0 ? errors.First() : null;
        }

        public static string IsValidChangePasswordInput(ChangePasswordDTO dto, string userHashedPassword)
        {
            ChangePasswordValidator validator = new ChangePasswordValidator(dto, userHashedPassword);

            var errors = validator.Validate();

            return errors.Count() > 0 ? errors.First() : null;
        }

        public static User Login(string email, string password, bool isRemembered = false)
        {
            return LoginHandler.Login(email, password);
        }

        public static User Register(RegisterDTO dto)
        {
            User user = UserFactory.Create(dto);
            user = UserRepository.Register(user);
            return user;
        }

        public static void UpdatePassword(User user, string recentPassword)
        {
            user.Password = SecurityHelper.Hash(recentPassword);
            UserHandler.UpdatePassword(user);
        }
    }
}