﻿using ReaIVycle.app.Handler;
using ReaIVycle.app.Model;
using System.Collections.Generic;

namespace ReaIVycle.app.Controller
{
    public static class UserController
    {
        public static User FindUserByID(int id)
        {
            return UserHandler.FindUserByID(id);
        }

        public static List<User> GetAllUsers()
        {
            return UserHandler.GetAllUsers();
        }

        public static void Promote(string id)
        {
            UserHandler.Promote(int.Parse(id));
        }

        public static void Delete(string id)
        {
            UserHandler.Delete(int.Parse(id));
        }
    }
}