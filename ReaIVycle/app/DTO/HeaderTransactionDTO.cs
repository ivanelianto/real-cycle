﻿using ReaIVycle.app.Model;
using System;

namespace ReaIVycle.app.DTO
{
    public class HeaderTransactionDTO
    {
        public int ID { get; set; }
        public DateTime Occurrence { get; set; }
        public User User { get; set; }
        public string Status { get; set; }
    }
}