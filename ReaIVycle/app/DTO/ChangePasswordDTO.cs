﻿using ReaIVycle.app.ValueObject;

namespace ReaIVycle.app.DTO
{
    public class ChangePasswordDTO
    {
        public Password Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}