﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.DTO
{
    public class DetailTransactionDTO
    {
        public int HeaderID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }
}