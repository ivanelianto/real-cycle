﻿using System;

namespace ReaIVycle.app.DTO
{
    public class AddToCartDTO
    {
        public readonly string Quantity;
        public readonly string Stock;

        public AddToCartDTO(string quantity, string stock)
        {
            this.Quantity = quantity;
            this.Stock = stock;
        }

        public AddToCartDTO Clone()
        {
            return new AddToCartDTO(this.Quantity, this.Stock);
        }

    }
}