﻿namespace ReaIVycle.app.DTO
{
    public class ProductDTO
    {
        public string ID { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Stock { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
    }
}
