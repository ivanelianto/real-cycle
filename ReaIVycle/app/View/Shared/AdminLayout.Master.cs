﻿using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View.Shared
{
    public partial class AdminLayout : System.Web.UI.MasterPage
    {
        public User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["user"] != null)
            {
                HttpCookie userCookie = Request.Cookies["user"];

                if (userCookie.Value != "" && userCookie.Expires >= DateTime.Now)
                    Session["user"] = UserRepository.FindByID(int.Parse(userCookie.Value));
            }

            if (Session["user"] != null)
                user = (User)Session["user"];

            if (user == null)
            {
                Response.RedirectToRoute("LoginRoute");
            }
        }
    }
}