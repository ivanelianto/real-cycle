﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.Model;
using System;
using System.Web;
using System.Linq;
using System.Diagnostics;

namespace ReaIVycle
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        public User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["user"] != null)
            {
                HttpCookie userCookie = Request.Cookies["user"];

                if (userCookie.Value != "" && userCookie.Expires >= DateTime.Now)
                    Session["user"] = UserController.FindUserByID(int.Parse(userCookie.Value));
            }

            if (Session["user"] != null)
            {
                user = (User)Session["user"];

                Debug.WriteLine(user);

                var itemsInCart = TransactionController.GetCurrentUserCart(user);

                if (itemsInCart != null && itemsInCart.DetailTransactions.Count > 0)
                    txtCartItemCount.Text = itemsInCart.DetailTransactions
                        .Sum(x => x.Quantity)
                        .ToString();
            }
        }
    }
}