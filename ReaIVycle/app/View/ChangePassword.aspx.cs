﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.DTO;
using ReaIVycle.app.Model;
using ReaIVycle.app.ValueObject;
using System;

namespace ReaIVycle.app.View
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
                user = (User)Session["user"];
            else
                Response.RedirectToRoute("LoginRoute");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Password password = new Password(txtCurrentPassword.Text, txtNewPassword.Text);

            ChangePasswordDTO dto = new ChangePasswordDTO()
            {
                Password = password,
                ConfirmPassword = txtConfirmNewPassword.Text
            };

            var error = AuthController.IsValidChangePasswordInput(dto, user.Password);

            if (!string.IsNullOrEmpty(error))
            {
                Session["error"] = error;
            }
            else
            {
                AuthController.UpdatePassword(user, dto.Password.Recent);

                Session["message"] = "Password changed successfully.";
            }
        }
    }
}