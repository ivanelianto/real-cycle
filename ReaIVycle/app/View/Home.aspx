﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="ReaIVycle.Home" %>

<%@ Import Namespace="ReaIVycle.app.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Home
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link rel="stylesheet" href="../../public/assets/css/home.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" runat="server">
        <section id="banner">
            <div class="slider-wrapper">
                <div class="slider">
                    <asp:Image
                        ImageUrl="~/public/images/bicycle/home-sliders/1.jpg"
                        ID="img1"
                        runat="server" />

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 aic">
                                <h5>Bike Collection</h5>

                                <h1 class="caption">New Bike
                                <br />
                                    From Real Cycle  
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slider">
                    <asp:Image
                        ImageUrl="~/public/images/bicycle/home-sliders/2.jpg"
                        ID="img2"
                        runat="server" />

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 df fdc aife jcc mt-4">
                                <h5>Bike Collection</h5>

                                <h1 class="caption">Durable Mountain Bike
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="slider">
                    <asp:Image
                        ImageUrl="~/public/images/bicycle/home-sliders/3.jpg"
                        ID="img3"
                        runat="server" />

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 fdc aifs">
                                <h5>Bike Collection</h5>

                                <h1 class="caption">Comfortable City Bike
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="products">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 df jcc">
                        <h1 class="section-title mt-4">Our Products</h1>
                    </div>
                </div>

                <div class="row">
                    <asp:Repeater
                        OnItemDataBound="productCards_ItemDataBound"
                        ID="productCards"
                        runat="server">
                        <ItemTemplate>
                            <div class="col-md-4">
                                <asp:LinkButton
                                    OnClick="btnProductDetail_Click"
                                    class="card df fdc aic"
                                    ID="btnProductDetail"
                                    runat="server">

                                    <div class="product-image-wrapper">
                                        <asp:Image
                                            ID="productImage"
                                            runat="server" />
                                    </div>

                                    <asp:Label
                                        class="product-name"
                                        ID="lblProductName"
                                        runat="server" />

                                    <asp:Label
                                        class="product-price"
                                        ID="lblProductPrice"
                                        runat="server" />

                                </asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <div class="row">
                    <a class="col-md-12 df jcc see-more-product"
                        href="/products">
                        See More
                    </a>
                </div>
            </div>
        </section>

        <section id="testimonials">
            <!-- <div class="container">
                <div class="row">
                    <div class="col-md-12 df jcc">
                        <h1 class="section-title mt-4">Testimonials</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="card">

                        </div>
                    </div>
                </div>
            </div> -->
        </section>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
    <script>
        $(window).on("load", () => {
            let header = $("header");
            header.addClass("home-page-header");
        });
    </script>
</asp:Content>
