﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.Model;
using ReaIVycle.app.Util;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View
{
    public partial class Cart : System.Web.UI.Page
    {
        private double grandtotal = 0;

        private List<DetailTransaction> itemsInCart = new List<DetailTransaction>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var user = (User)Session["user"];

                if (user == null || (user != null && user.Role == "Admin"))
                    Response.RedirectToRoute("LoginRoute");
                else
                {
                    var header = TransactionController.GetCurrentUserCart();

                    if (header != null)
                    {
                        foreach (DetailTransaction detail in header.DetailTransactions)
                            itemsInCart.Add(detail);

                        InitData();

                        lblGrandtotal.Text = StringFormatter.GetCurrenciesFormat(grandtotal);

                        if (header.DetailTransactions.Count > 0)
                            btnCheckout.Visible = true;
                    }
                }
            }
        }

        private void InitData()
        {
            cartItems.DataSource = itemsInCart;
            cartItems.DataBind();
        }

        protected void productCards_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DetailTransaction detail = (DetailTransaction)e.Item.DataItem;

            Product product = detail.Product;

            HyperLink link = (HyperLink)e.Item.FindControl("btnProductDetail");
            link.NavigateUrl = GetRouteUrl("ProductDetailRoute", new { id = product.ID });

            Image image = (Image)e.Item.FindControl("productImage");
            image.ImageUrl = product.GetPicturePath();

            Image categoryIcon = (Image)e.Item.FindControl("icoCategory");
            categoryIcon.ImageUrl = product.GetCategoryIconPath();

            Label productName = (Label)e.Item.FindControl("lblProductName");
            productName.Text = product.Name;

            Label productPrice = (Label)e.Item.FindControl("lblProductPrice");
            productPrice.Text = product.GetFormattedPrice();

            Label quantity = (Label)e.Item.FindControl("lblQuantity");
            quantity.Text = detail.Quantity + " item(s).";

            double subtotal = (double)(detail.Quantity * product.Price);
            Label lblSubtotal = (Label)e.Item.FindControl("lblSubtotal");
            lblSubtotal.Text = "Subtotal : " + StringFormatter.GetCurrenciesFormat(subtotal);

            grandtotal += subtotal;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            LinkButton btnDelete = sender as LinkButton;

            var primaryKeyValues = btnDelete.CommandArgument.Split(',');
            var headerID = int.Parse(primaryKeyValues[0]);
            var productID = int.Parse(primaryKeyValues[1]);

            TransactionController.RemoveItem(headerID, productID);

            Response.Redirect("/cart");
        }

        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            TransactionController.Checkout();

            Response.Redirect("/cart");
        }
    }
}