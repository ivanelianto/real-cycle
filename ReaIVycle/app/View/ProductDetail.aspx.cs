﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.DTO;
using ReaIVycle.app.Model;
using ReaIVycle.app.Util;
using System;
using System.Collections.Generic;

namespace ReaIVycle.app.View
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        protected Product product;

        protected List<Category> categories = new List<Category>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var currentUser = (User)Session["user"];

                if (currentUser != null && currentUser.Role == "Admin")
                    btnEdit.Visible = true;
                else if (currentUser != null && currentUser.Role == "Member")
                {
                    btnAddToCart.Visible = true;
                    quantityPanel.Visible = true;
                }
            }

            int id = int.Parse(URLHelper.GetIDForDetailPage(this));
            product = ProductController.FindProductByID(id);

            productImage.ImageUrl = product.GetPicturePath();
            txtCategory.Text = product.Category.Name;
            txtName.Text = product.Name;
            txtPrice.Text = product.Price.ToString();
            txtStock.Text = product.Stock.ToString();
            txtDescription.Text = product.Description;
        }

        protected void btnAddToCart_ServerClick(object sender, EventArgs e)
        {
            AddToCartDTO dto = new AddToCartDTO(txtQuantity.Text, txtStock.Text);

            var error = TransactionController.IsValidAddToCartInput(product.ID, dto);

            if (!string.IsNullOrEmpty(error))
                Session["error"] = error;
            else
            {
                TransactionController.AddTransaction(product, dto.Quantity);

                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        protected void btnEdit_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect(GetRouteUrl("AdminProductDetailRoute", new { id = product.ID }));
        }
    }
}