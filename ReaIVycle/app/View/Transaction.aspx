﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Transaction.aspx.cs" Inherits="ReaIVycle.app.View.Transaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Transactions
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link rel="stylesheet" href="/public/assets/css/transaction.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" runat="server">
        <div class="container main-container">
            <%
                if (user != null && user.Role == "Admin")
                {
            %>
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <asp:Button
                        CssClass="btn btn-primary generate-button"
                        ID="btnGenerateReport"
                        OnClick="btnGenerateReport_Click"
                        Text="Generate Report"
                        runat="server" />
                </div>
            </div>
            <%
                }
            %>

            <div class="row">
                <div class="col-md-12">
                    <table>
                        <tr class="table-header">
                            <th>#</th>

                            <%
                                if (user != null && user.Role == "Admin")
                                {
                            %>
                            <th>User Name</th>
                            <%
                                }
                            %>

                            <th>Occurrence</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>

                        <asp:Repeater
                            ID="transactionList"
                            OnItemDataBound="transactionList_ItemDataBound"
                            runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNumber" runat="server" /></td>

                                    <%
                                        if (user != null && user.Role == "Admin")
                                        {
                                    %>
                                    <td>
                                        <asp:Label ID="lblUserName" runat="server" /></td>
                                    <%
                                        }
                                    %>

                                    <td>
                                        <asp:Label ID="lblOccurrence" runat="server" />
                                    </td>

                                    <td>
                                        <asp:Label CssClass="status" ID="lblStatus" runat="server" />
                                    </td>

                                    <td>
                                        <asp:Button 
                                            ID="btnViewDetail"
                                            Text="View Detail"
                                            CssClass="btn btn-primary"
                                            OnClick="btnViewDetail_Click"
                                            runat="server"/>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
