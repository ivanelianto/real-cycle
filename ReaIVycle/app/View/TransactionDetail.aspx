﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="TransactionDetail.aspx.cs" Inherits="ReaIVycle.app.View.TransactionDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Transaction Detail
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link rel="stylesheet" href="/public/assets/css/transaction-detail.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" runat="server">
        <div class="container main-container">
            <div class="row transaction-header">
                <div class="col-md-6">
                    <div class="transaction-id-wrapper">
                        <div class="row">
                            <div class="col-md-4">Transaction ID</div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblID" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div class="user-name-wrapper">
                        <div class="row">
                            <div class="col-md-4">User Name</div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblUserName" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="occurrence-wrapper">
                        <div class="row">
                            <div class="col-md-4">Occurrence</div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblOccurrence" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div class="status-wrapper">
                        <div class="row">
                            <div class="col-md-4">Status</div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-7">
                                <asp:Label ID="lblStatus" runat="server" />
                            </div>
                        </div>

                        <asp:Panel CssClass="row" 
                            ID="btnApproveWrapper"
                            Visible="false"
                            runat="server">
                            <div class="col-md-5"></div>
                            <div class="col-md-7 button-approve-wrapper">
                                <asp:Button
                                    ID="btnApprove"
                                    CssClass="btn btn-primary"
                                    Text="Approve"
                                    OnClick="btnApprove_Click"
                                    runat="server" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>

            <div class="row">
                <table>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Product Price</th>
                        <th>Subtotal</th>
                    </tr>

                    <asp:Repeater
                        ID="detailList"
                        OnItemDataBound="detailList_ItemDataBound"
                        runat="server">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNumber" runat="server" /></td>
                                <td>
                                    <asp:Label ID="lblProductName" runat="server" /></td>
                                <td>
                                    <asp:Label ID="lblQuantity" runat="server" /></td>
                                <td>
                                    <asp:Label ID="lblPrice" runat="server" /></td>
                                <td>
                                    <asp:Label ID="lblSubtotal" runat="server" /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>

                    <tr>
                        <td colspan="3"></td>
                        <td>Grandtotal</td>
                        <td>
                            <asp:Label
                                ID="lblGrandtotal"
                                class="grandtotal"
                                runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
