﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View
{
    public partial class ProductPage : System.Web.UI.Page
    {
        protected static List<Category> categories = new List<Category>();
        protected static List<Product> products = new List<Product>();

        protected User currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            currentUser = (User)Session["user"];

            if (currentUser != null && currentUser.Role == "Admin")
                btnAddNewProduct.Visible = true;

            if (!IsPostBack)
            {
                txtMessage.Text = "";

                InitProductData();
                InitCategoryData();
            }
            else if (IsPostBack && Session["message"] != null)
            {
                txtMessage.Text = Session["message"].ToString();
                Session.Remove("message");
            }
        }

        private void InitCategoryData()
        {
            categories = CategoryController.GetAllCategories();
            Category chooseSelectionText = CategoryFactory.Create("-- Select --");
            categories.Insert(0, chooseSelectionText);
            ddlCategory.DataSource = categories;
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();
        }

        private void InitProductData()
        {
            products = ProductController.GetAllProducts();
            productCards.DataSource = products;
            productCards.DataBind();
        }

        private void InitProductDataWithCategoryFilter(int categoryID)
        {
            if (categoryID == 0)
            {
                InitProductData();
            }
            else
            {
                products = ProductController.FindProductByCategory(categoryID);
                productCards.DataSource = products;
                productCards.DataBind();
            }
        }

        protected void btnProductDetail_Click(object sender, EventArgs e)
        {
            LinkButton linkButton = sender as LinkButton;
            Response.RedirectToRoute(linkButton.PostBackUrl);
        }

        protected void productCards_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Product product = (Product)e.Item.DataItem;

            HyperLink link = (HyperLink)e.Item.FindControl("btnProductDetail");
            link.NavigateUrl = GetRouteUrl("ProductDetailRoute", new { id = product.ID });

            Image image = (Image)e.Item.FindControl("productImage");
            image.ImageUrl = product.GetPicturePath();

            Image categoryIcon = (Image)e.Item.FindControl("icoCategory");
            categoryIcon.ImageUrl = product.GetCategoryIconPath();

            Label productName = (Label)e.Item.FindControl("lblProductName");
            productName.Text = product.Name;

            Label productPrice = (Label)e.Item.FindControl("lblProductPrice");
            productPrice.Text = product.GetFormattedPrice();

            Label productStock = (Label)e.Item.FindControl("lblProductStock");
            productStock.Text = product.Stock.Value + " unit(s) available";

            HyperLink btnEdit = (HyperLink)e.Item.FindControl("btnEdit");
            btnEdit.NavigateUrl = GetRouteUrl("AdminProductDetailRoute", new { id = product.ID });

            LinkButton btnDelete = (LinkButton)e.Item.FindControl("btnDelete");
            btnDelete.CommandArgument = product.ID + "";
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int categoryID = int.Parse(ddlCategory.SelectedValue.ToString());
            InitProductDataWithCategoryFilter(categoryID);
        }

        protected void productCards_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "deleteProduct")
            {
                ProductController.DeleteProduct(e.CommandArgument.ToString());

                Session["message"] = "Product deleted successfully.";
                Response.RedirectToRoute("ProductRoute");
            }
        }

        protected void productCards_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            Panel adminActionButtonPanel = (Panel)e.Item.FindControl("adminActionButtonPanel");

            if (currentUser != null && currentUser.Role == "Admin")
                adminActionButtonPanel.Visible = true;
        }
    }
}
