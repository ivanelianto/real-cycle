﻿using ReaIVycle.app.Model;
using System;

namespace ReaIVycle.app.View
{
    public partial class Profile : System.Web.UI.Page
    {
        protected User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] != null)
                user = (User)Session["user"];
            else
                Response.Redirect("/login");
        }
    }
}