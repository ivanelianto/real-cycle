﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View.Auth
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie userCookie = Request.Cookies["user"];

            if (userCookie != null)
            {
                userCookie.Value = "";
                userCookie.Expires = DateTime.Now.AddYears(-1);
                Response.SetCookie(userCookie);
            }

            Session.Abandon();

            if (Request.UrlReferrer == null)
                Response.RedirectToRoute("Default");
            else
                Response.Redirect(Request.UrlReferrer.ToString());
        }
    }
}