﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ReaIVycle.app.View.Auth.Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Register
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link rel="stylesheet" href="../../../public/assets/css/register.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="container">
        <form method="post" runat="server">
            <%
                if (Session["error"] != null)
                {
            %>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <p class="alert alert-danger">
                        <% 
                            Response.Write(Session["error"].ToString());
                            Session.Remove("error");
                        %>
                    </p>
                </div>
                <div class="col-md-3"></div>
            </div>
            <%
                }
            %>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <asp:TextBox placeholder="Name" ID="txtName" runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <asp:TextBox placeholder="Email" TextMode="Email" ID="txtEmail" runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <asp:TextBox placeholder="Password" TextMode="Password" ID="txtPassword" runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <asp:TextBox placeholder="Confirm Password" TextMode="Password" ID="txtConfirmPassword"
                            runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <p>Gender</p>

                        <div class="radio-group-wrapper df mb-3">
                            <div class="radio-content">
                                <asp:RadioButton GroupName="Gender" Text="Male" ID="rbMale" runat="server" />
                            </div>

                            <div class="radio-content">
                                <asp:RadioButton GroupName="Gender" Text="Female" ID="rbFemale" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <asp:TextBox placeholder="Birthday" TextMode="Date" ID="txtBirthday" runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <asp:TextBox placeholder="Phone Number" ID="txtPhone" runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="input-wrapper">
                        <asp:TextBox placeholder="Address" Rows="5" ID="txtAddress" runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="row">
                        <asp:Button class="btn btn-primary" OnClick="btnRegister_Click" Text="Register" ID="btnRegister"
                            runat="server" />
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </form>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>