﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.DTO;
using System;

namespace ReaIVycle.app.View.Auth
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["user"] != null)
                    Response.RedirectToRoute("Default");
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            string gender = "";

            if (rbMale.Checked)
                gender = "Male";
            else if (rbFemale.Checked)
                gender = "Female";

            RegisterDTO dto = new RegisterDTO()
            {
                Name = txtName.Text,
                Email = txtEmail.Text,
                Password = txtPassword.Text,
                ConfirmPassword = txtConfirmPassword.Text,
                Gender = gender,
                Birthday = txtBirthday.Text,
                Phone = txtPhone.Text,
                Address = txtAddress.Text
            };

            string error = AuthController.IsValidRegistrationInput(dto);

            if (!string.IsNullOrEmpty(error))
            {
                Session["error"] = error;
            }
            else
            {
                var user = AuthController.Register(dto);
                Session["user"] = user;
                Response.RedirectToRoute("Default");
            }
        }
    }
}