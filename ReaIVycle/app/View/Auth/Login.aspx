﻿<%@ Import Namespace="ReaIVycle.app.Util" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ReaIVycle.app.View.Auth.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Login
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link rel="stylesheet" href="../../../public/assets/css/login.css"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" runat="server" class="login-form">
                    <%
                        if (Session["error"] != null)
                        {
                    %>
                            <p class="alert alert-danger">
                                <%
                                    Response.Write(Session["error"].ToString());
                                    Session.Remove("error");
                                %>
                            </p>
                    <%
                        }
                    %>

                    <asp:TextBox 
                        class="input-wrapper mb-4"
                        placeholder="Email"
                        ID="txtEmail"
                        runat="server"/>

                    <asp:TextBox 
                        class="input-wrapper mb-4"
                        placeholder="Password"
                        TextMode="Password"
                        ID="txtPassword"
                        runat="server"/>

                    <div class="checkbox-wrapper mb-4">
                        <asp:CheckBox
                            Text="Remember Me"
                            ID="cbRememberMe"
                            runat="server"/>
                    </div>

                    <asp:Button
                        class="btn btn-primary"
                        Text="Login"
                        OnClick="btnSubmit_Click"
                        ID="btnSubmit"
                        runat="server"/>
                </form>
            </div>
        </div>
    </div>    
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
