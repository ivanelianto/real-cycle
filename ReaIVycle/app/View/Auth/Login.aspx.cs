﻿using ReaIVycle.app.Util;
using ReaIVycle.app.Model;
using ReaIVycle.app.Controller;
using System;
using System.Web;

namespace ReaIVycle.app.View.Auth
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["user"] != null)
                    Response.RedirectToRoute("Default");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string email = txtEmail.Text;
            string password = txtPassword.Text;

            if (!AuthController.IsValidLoginInput(email, password))
                SessionManager.GetInstance().SetErrorMessage("Email and password must be filled.");
            else
            {
                bool isRemember = cbRememberMe.Checked;

                User user = AuthController.Login(email, password, isRemember);

                if (user != null)
                {
                    Session["user"] = user;

                    if (cbRememberMe.Checked)
                    {
                        HttpCookie cookie = new HttpCookie("user", user.ID.ToString());
                        cookie.Expires = DateTime.Now.AddHours(1);
                        Response.Cookies.Add(cookie);
                    }

                    Response.RedirectToRoute("Default");
                }
                else
                {
                    Session["error"] = "Email or password is incorrect.";
                }
            }
        }
    }
}