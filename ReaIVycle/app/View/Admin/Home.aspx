﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/AdminLayout.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="ReaIVycle.app.View.Shared.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Admin Home
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link href="../../../public/assets/css/admin-home.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="container main-container df jcc aic">
        <div class="row">
            <div class="col-md-12 df fdc jcc aic">
                <h1>
                    Welcome To Administrator Page 
                </h1>
                <h3 class="mt-4">
                    Choose Any Action From Navigation Bar Above
                </h3>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
