﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Create.aspx.cs" Inherits="ReaIVycle.app.View.Admin.ManageProduct.Create" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Admin | Add New Product
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link href="../../../../public/assets/css/product-create.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" enctype="multipart/form-data" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <asp:Label
                        ID="txtMessage"
                        runat="server" />

                    <div class="input-wrapper">
                        <asp:TextBox
                            placeholder="Name"
                            ID="txtName"
                            runat="server" />
                        <label class="hint" for="name">Name</label>
                    </div>

                    <div class="input-wrapper">
                        <asp:DropDownList
                            placeholder="Category"
                            ID="ddlCategory"
                            runat="server" />
                        <label class="hint" for="ddlCategory">Category</label>
                    </div>

                    <div class="input-wrapper">
                        <asp:TextBox
                            placeholder="Price"
                            ID="txtPrice"
                            runat="server" />
                        <label class="hint" for="txtPrice">Price</label>
                    </div>

                    <div class="input-wrapper">
                        <asp:TextBox
                            placeholder="Stock"
                            ID="txtStock"
                            runat="server" />
                        <label class="hint" for="txtStock">Stock</label>
                    </div>

                    <div class="input-wrapper">
                        <asp:TextBox
                            placeholder="Description"
                            TextMode="MultiLine"
                            ID="txtDescription"
                            runat="server" />
                        <label class="hint" for="txtDescription">Description</label>
                    </div>

                    <div class="input-wrapper">
                        <asp:FileUpload
                            ID="imageFileUpload"
                            runat="server" />
                            
                        <label class="hint" for="imageFileUpload">Product Image</label>
                    </div>

                    <button
                        class="btn btn-primary"
                        onserverclick="btnAdd_Click"
                        id="btnAdd"
                        runat="server">
                        <i class="fas fa-plus"></i>
                        Add New Product
                    </button>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
