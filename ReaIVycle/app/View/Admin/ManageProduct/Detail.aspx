﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="ReaIVycle.app.View.Admin.ManageProduct.Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Admin | Product Detail
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link href="../../../../public/assets/css/admin-product-detail.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" enctype="multipart/form-data" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <asp:Label
                        ID="txtMessage"
                        runat="server"/>
                        
                    <div class="card">
                        <div class="product-image-wrapper">
                            <asp:Image
                                ID="productImage"
                                runat="server" />
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox
                                placeholder="Name"
                                ID="txtName" 
                                runat="server"/>
                            <label class="hint" for="name">Name</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:DropDownList 
                                OnDataBound="ddlCategory_DataBound"
                                placeholder="Category"
                                ID="ddlCategory"
                                runat="server"/>
                            <label class="hint" for="ddlCategory">Category</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox 
                                ID="txtPrice" 
                                runat="server"/>
                            <label class="hint" for="txtPrice">Price</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox 
                                ID="txtStock" 
                                runat="server"/>
                            <label class="hint" for="txtStock">stock</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox 
                                placeholder="Description"
                                TextMode="MultiLine"
                                ID="txtDescription" 
                                runat="server"/>
                            <label class="hint" for="txtDescription">Description</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:FileUpload 
                                ID="imageFileUpload" 
                                runat="server"/>
                            <label class="hint" for="imageFileUpload">Product Image</label>
                        </div>

                        <div class="action-wrapper">
                            <button
                                class="btn btn-primary"
                                onserverclick="btnUpdate_Click"
                                id="btnUpdate"
                                runat="server">
                                <i class="fas fa-edit"></i>
                                Update
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
