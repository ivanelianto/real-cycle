﻿using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using ReaIVycle.app.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View.Admin.ManageProduct
{
    public partial class Index : System.Web.UI.Page
    {
        protected static List<Category> categories = new List<Category>();
        protected static List<Product> products = new List<Product>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtMessage.Text = "";

                InitProductData();
                InitCategoryData();
            }
            else if (IsPostBack && Session["message"] != null)
            {
                txtMessage.Text = Session["message"].ToString();
                Session.Remove("message");
            }
        }

        private void InitCategoryData()
        {
            categories = CategoryRepository.GetAllCategories();
            Category chooseSelectionText = CategoryFactory.Create("-- Select --");
            categories.Insert(0, chooseSelectionText);
            ddlCategory.DataSource = categories;
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataBind();
        }

        private void InitProductData()
        {
            products = ProductRepository.GetAllProducts();
            productCards.DataSource = products;
            productCards.DataBind();
        }

        private void InitProductDataWithCategoryFilter(int categoryID)
        {
            products = ProductRepository.FindByCategory(categoryID);
            productCards.DataSource = products;
            productCards.DataBind();
        }

        protected void btnAddNewProduct_ServerClick(object sender, EventArgs e)
        {
            Response.RedirectToRoute("CreateProductRoute");
        }

        protected void btnProductDetail_Click(object sender, EventArgs e)
        {
            LinkButton linkButton = sender as LinkButton;
            Response.RedirectToRoute(linkButton.PostBackUrl);
        }

        protected void productCards_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Product product = (Product)e.Item.DataItem;

            LinkButton link = (LinkButton)e.Item.FindControl("btnProductDetail");
            link.PostBackUrl = GetRouteUrl("AdminProductDetailRoute", new { id = product.ID });

            Image image = (Image)e.Item.FindControl("productImage");
            image.ImageUrl = product.GetPicturePath();

            Label productName = (Label)e.Item.FindControl("lblProductName");
            productName.Text = product.Name;

            Label productPrice = (Label)e.Item.FindControl("lblProductPrice");
            productPrice.Text = product.GetFormattedPrice();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int categoryID = int.Parse(ddlCategory.SelectedValue.ToString());
            InitProductDataWithCategoryFilter(categoryID);
        }
    }
}