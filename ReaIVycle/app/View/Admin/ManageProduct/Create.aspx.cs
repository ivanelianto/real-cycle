﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.DTO;
using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using ReaIVycle.app.Validator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ReaIVycle.app.View.Admin.ManageProduct
{
    public partial class Create : System.Web.UI.Page
    {
        protected Product product;

        protected List<Category> categories = new List<Category>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtMessage.Text = "";

                InitCategoryData();
            }
        }

        private void InitCategoryData()
        {
            categories = CategoryController.GetAllCategories();
            Category choose = CategoryFactory.Create("-- Choose --");
            categories.Insert(0, choose);
            ddlCategory.DataSource = categories;
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ProductDTO dto = new ProductDTO()
            {
                Category = ddlCategory.SelectedValue,
                Name = txtName.Text,
                Price = txtPrice.Text,
                Stock = txtStock.Text,
                ImagePath = imageFileUpload.FileName,
                Description = txtDescription.Text
            };

            var error = ProductController.IsValidInput(dto);

            if (!string.IsNullOrEmpty(error))
            {
                txtMessage.Text = error;
                txtMessage.CssClass = "alert alert-danger";
            }
            else
            {
                Product product = ProductFactory.Create(dto);

                string fileExtension = Path.GetExtension(imageFileUpload.FileName);
                string finalFileName = product.Name + fileExtension;
                string dst = Server.MapPath("/public/images/uploads/" + finalFileName);
                imageFileUpload.SaveAs(dst);

                product.PicturePath = finalFileName;
                ProductController.AddProduct(product);

                Session["message"] = "New Product Added Successfully.";
                Response.RedirectToRoute("ProductRoute");
            }
        }
    }
}