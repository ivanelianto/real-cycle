﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.DTO;
using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using ReaIVycle.app.Util;
using System;
using System.Collections.Generic;
using System.IO;

namespace ReaIVycle.app.View.Admin.ManageProduct
{
    public partial class Detail : System.Web.UI.Page
    {
        protected Product product;

        protected List<Category> categories = new List<Category>();

        private int currentProductId;

        protected void Page_Load(object sender, EventArgs e)
        {
            var user = (User)Session["user"];

            if (user == null || (user != null && user.Role != "Admin"))
                Response.RedirectToRoute("DefaultRoute");

            txtMessage.Text = "";

            if (!IsPostBack)
            {
                InitCategoryData();
                currentProductId = int.Parse(URLHelper.GetIDForEditPage(this));
                product = ProductController.FindProductByID(currentProductId);

                productImage.ImageUrl = product.GetPicturePath();
                txtName.Text = product.Name;
                txtPrice.Text = product.Price.ToString();
                txtStock.Text = product.Stock.ToString();
                txtDescription.Text = product.Description;
            }
        }

        private void InitCategoryData()
        {
            categories = CategoryController.GetAllCategories();
            Category choose = CategoryFactory.Create("-- Choose --");
            categories.Insert(0, choose);
            ddlCategory.DataSource = categories;
            ddlCategory.DataValueField = "ID";
            ddlCategory.DataTextField = "Name";
            ddlCategory.DataBind();
        }

        protected void ddlCategory_DataBound(object sender, EventArgs e)
        {
            currentProductId = int.Parse(URLHelper.GetIDForEditPage(this));
            Product product = ProductController.FindProductByID(currentProductId);

            if (product != null)
                ddlCategory.SelectedValue = product.Category.ID.ToString();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            ProductDTO dto = new ProductDTO()
            {
                ID = URLHelper.GetIDForEditPage(this),
                Category = ddlCategory.SelectedValue,
                Name = txtName.Text,
                Price = txtPrice.Text,
                Stock = txtStock.Text,
                ImagePath = productImage.ImageUrl,
                Description = txtDescription.Text
            };

            var error = ProductController.IsValidInput(dto);

            if (!string.IsNullOrEmpty(error))
            {
                txtMessage.Text = error;
                txtMessage.CssClass = "alert alert-danger";
            }
            else
            {
                Product product = ProductFactory.Create(dto);

                if (imageFileUpload.FileName != "")
                {
                    string fileExtension = Path.GetExtension(imageFileUpload.FileName);
                    string finalFileName = product.Name + fileExtension;
                    string dst = Server.MapPath("/public/images/uploads/" + finalFileName);
                    imageFileUpload.SaveAs(dst);

                    product.PicturePath = finalFileName;
                }

                ProductController.Update(product);

                txtMessage.Text = "Product Updated Successfully.";
                txtMessage.CssClass = "alert alert-success";
            }
        }
    }
}