﻿<%@ Import Namespace="ReaIVycle.app.Model" %>

<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/AdminLayout.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ReaIVycle.app.View.Admin.ManageProduct.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Admin | Product
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link href="../../../../public/assets/css/admin-product.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" runat="server">
        <% 
            if (Session["message"] != null)
            {
        %>
                <asp:Label
                    class="alert"
                    ID="txtMessage"
                    runat="server" />
        <% 
            } 
        %>
        <div class="container main-container">
            <div class="row">
                <div class="col-md-5"></div>

                <div class="col-md-7 product-header">
                    <label>Filter by Category</label>

                    <span class="category-combobox-wrapper">
                        <asp:DropDownList 
                            AutoPostBack="true"
                            OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                            ID="ddlCategory" 
                            class="category-combobox" 
                            runat="server" />
                    </span>
                </div>
            </div>

            <div class="row">
                <asp:Repeater
                    OnItemDataBound="productCards_ItemDataBound"
                    ID="productCards"
                    runat="server">
                    <ItemTemplate>
                        <div class="col-md-3">
                            <asp:LinkButton
                                OnClick="btnProductDetail_Click"
                                class="card df fdc aic"
                                ID="btnProductDetail"
                                runat="server">

                                <div class="product-image-wrapper">
                                    <asp:Image
                                        ID="productImage"
                                        runat="server" />
                                </div>

                                <asp:Label
                                    class="product-name"
                                    ID="lblProductName"
                                    runat="server" />

                                <asp:Label
                                    class="product-price"
                                    ID="lblProductPrice"
                                    runat="server" />

                            </asp:LinkButton>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
