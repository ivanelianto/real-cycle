﻿<%@ Import Namespace="ReaIVycle.app.Model" %>
<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="ReaIVycle.app.View.Admin.ManageUser.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Admin | User
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link href="../../../../public/assets/css/admin-user.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" runat="server">
        <div class="container main-container">
            <div class="row">
                <div class="col-md-12 df jcc aic">
                    <table>
                        <tr class="table-header">
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>

                        <asp:Repeater
                            OnItemDataBound="userList_ItemDataBound"
                            ID="userList"
                            runat="server">

                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label
                                            ID="lblRowNumber"
                                            runat="server" />
                                    </td>

                                    <td>
                                        <asp:Label
                                            ID="lblName"
                                            runat="server" />
                                    </td>

                                    <td>
                                        <asp:Label
                                            ID="lblEmail"
                                            runat="server" />
                                    </td>

                                    <td>
                                        <asp:Label
                                            ID="lblPhone"
                                            runat="server" />
                                    </td>

                                    <td>
                                        <asp:Label
                                            ID="lblRole"
                                            runat="server" />
                                    </td>

                                    <td>
                                        <asp:Button
                                            CommandArgument='<%# Eval("ID") %>'
                                            OnClick="btnDelete_Click"
                                            CssClass="btn btn-danger"
                                            Text="Delete"
                                            ID="btnDelete"
                                            runat="server" />

                                        <asp:Button
                                            CommandArgument='<%# Eval("ID") %>'
                                            OnClick="btnSetAsAdmin_Click"
                                            CssClass="btn btn-primary"
                                            Text="Promote"
                                            ID="btnSetAsAdmin"
                                            runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
