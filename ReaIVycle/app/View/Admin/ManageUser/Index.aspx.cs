﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View.Admin.ManageUser
{
    public partial class Index : System.Web.UI.Page
    {
        protected List<User> users = new List<User>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                users = UserController.GetAllUsers();
                userList.DataSource = users;
                userList.DataBind();
            }
        }

        protected void userList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            User user = (User)e.Item.DataItem;

            Label lblRowNumber = ((Label)e.Item.FindControl("lblRowNumber"));
            lblRowNumber.Text = (1 + e.Item.ItemIndex).ToString();

            Label lblName = ((Label)e.Item.FindControl("lblName"));
            lblName.Text = user.Name;

            Label lblEmail = ((Label)e.Item.FindControl("lblEmail"));
            lblEmail.Text = user.Email;

            Label lblPhone = ((Label)e.Item.FindControl("lblPhone"));
            lblPhone.Text = user.Phone;

            Label lblRole = ((Label)e.Item.FindControl("lblRole"));
            lblRole.Text = user.Role;

            Button btnSetAsAdmin = ((Button)e.Item.FindControl("btnSetAsAdmin"));
            if (user.Role == "Admin")
                btnSetAsAdmin.Visible = false;
        }

        protected void btnSetAsAdmin_Click(object sender, EventArgs e)
        {
            Button btn = (sender as Button);

            UserController.Promote(btn.CommandArgument);

            Session["message"] = "User Promoted Successfully.";
            Response.RedirectToRoute("AdminUserRoute");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            Button btn = (sender as Button);
            UserController.Delete(btn.CommandArgument);

            Session["message"] = "User Deleted Successfully.";
            Response.RedirectToRoute("AdminUserRoute");
        }
    }
}