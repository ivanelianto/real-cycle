﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View
{
    public partial class Transaction : System.Web.UI.Page
    {
        public User user;

        private List<HeaderTransaction> transactions = new List<HeaderTransaction>();

        protected void Page_Load(object sender, EventArgs e)
        {
            user = (User)Session["user"];

            if (user == null)
            {
                Response.RedirectToRoute("LoginRoute");
            }
            else if (user != null && !IsPostBack)
            {
                if (user.Role == "Admin")
                    transactions = TransactionController.GetTransactions();
                else
                    transactions = TransactionController.FindTransactionsByUser(user);

                BindData();
            }
        }

        private void BindData()
        {
            transactionList.DataSource = transactions;
            transactionList.DataBind();
        }

        protected void transactionList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HeaderTransaction header = e.Item.DataItem as HeaderTransaction;

            Label lblNumber = (Label)e.Item.FindControl("lblNumber");
            lblNumber.Text = (e.Item.ItemIndex + 1).ToString();

            Label lblUserName = (Label)e.Item.FindControl("lblUserName");
            lblUserName.Text = header.User.Name;

            Label lblOccurrence = (Label)e.Item.FindControl("lblOccurrence");
            lblOccurrence.Text = header.Occurrence.Value.ToString("dddd, dd MMMM yyyy HH:mm");

            Label lblStatus = (Label)e.Item.FindControl("lblStatus");
            lblStatus.Text = header.Status;
            if (header.Status == TransactionFactory.PENDING_STATUS ||
                header.Status == TransactionFactory.IN_CART_STATUS)
            {
                lblStatus.CssClass += " text-danger";
            }
            else
            {
                lblStatus.CssClass += " text-success";
            }

            Button btnViewDetail = (Button)e.Item.FindControl("btnViewDetail");
            btnViewDetail.CommandArgument = header.ID.ToString();
        }

        protected void btnGenerateReport_Click(object sender, EventArgs e)
        {
            Response.RedirectToRoute("ReportRoute");
        }

        protected void btnViewDetail_Click(object sender, EventArgs e)
        {
            Button btnViewDetail = sender as Button;

            var route = GetRouteUrl("TransactionDetailRoute", new { id = btnViewDetail.CommandArgument });

            Response.Redirect(route);
        }
    }
}