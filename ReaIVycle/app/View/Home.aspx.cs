﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReaIVycle
{
    public partial class Home : Page
    {
        public List<Product> products = new List<Product>();

        protected void Page_Load(object sender, EventArgs e)
        {
            RefreshData();

            productCards.DataSource = products.Take(3);
            productCards.DataBind();
        }

        public void RefreshData()
        {
            this.products = ProductController.GetAllProducts();
        }

        protected void btnProductDetail_Click(object sender, EventArgs e)
        {
            LinkButton linkButton = sender as LinkButton;
            Response.RedirectToRoute(linkButton.PostBackUrl);
        }

        protected void productCards_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Product product = (Product)e.Item.DataItem;

            LinkButton link = (LinkButton)e.Item.FindControl("btnProductDetail");
            link.PostBackUrl = GetRouteUrl("ProductDetailRoute", new { id = product.ID });

            Image image = (Image)e.Item.FindControl("productImage");
            image.ImageUrl = product.GetPicturePath();

            Label productName = (Label)e.Item.FindControl("lblProductName");
            productName.Text = product.Name;

            Label productPrice = (Label)e.Item.FindControl("lblProductPrice");
            productPrice.Text = product.GetFormattedPrice();
        }
    }
}