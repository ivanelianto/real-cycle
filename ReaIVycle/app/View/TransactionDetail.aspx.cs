﻿using ReaIVycle.app.Controller;
using ReaIVycle.app.Factory;
using ReaIVycle.app.Model;
using ReaIVycle.app.Util;
using System;
using System.Web.UI.WebControls;

namespace ReaIVycle.app.View
{
    public partial class TransactionDetail : System.Web.UI.Page
    {
        private double grandtotal = 0;

        public HeaderTransaction header;

        protected void Page_Load(object sender, EventArgs e)
        {
            var user = (User)Session["user"];

            if (user == null)
                Response.RedirectToRoute("LoginRoute");
            else 
            {
                var id = int.Parse(URLHelper.GetIDForDetailPage(this));
                header = TransactionController.FindTransactionByID(id);

                if (!IsPostBack)
                {
                    lblID.Text = header.ID.ToString();
                    lblUserName.Text = header.User.Name;
                    lblOccurrence.Text = header.Occurrence.Value.ToString("dddd, dd MMMM yyyy HH:mm");
                    lblStatus.Text = header.Status;

                    detailList.DataSource = header.DetailTransactions;
                    detailList.DataBind();

                    lblGrandtotal.Text = StringFormatter.GetCurrenciesFormat(grandtotal);

                    if (user.Role == "Admin" &&
                        header.Status != TransactionFactory.APPROVED_STATUS)
                        btnApproveWrapper.Visible = true;
                }
            }
        }

        protected void detailList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DetailTransaction detail = e.Item.DataItem as DetailTransaction;

            Label lblNumber = e.Item.FindControl("lblNumber") as Label;
            lblNumber.Text = (e.Item.ItemIndex + 1).ToString();

            Label lblProductName = e.Item.FindControl("lblProductName") as Label;
            lblProductName.Text = detail.Product.Name;

            Label lblQuantity = e.Item.FindControl("lblQuantity") as Label;
            lblQuantity.Text = detail.Quantity.ToString();

            Label lblPrice = e.Item.FindControl("lblPrice") as Label;
            lblPrice.Text = StringFormatter.GetCurrenciesFormat(detail.Price.Value);

            double subtotal = (double)(detail.Price.Value * detail.Quantity);
            Label lblSubtotal = e.Item.FindControl("lblSubtotal") as Label;
            lblSubtotal.Text = StringFormatter.GetCurrenciesFormat(subtotal);

            grandtotal += subtotal;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            TransactionController.ApproveTransaction(header);

            Response.Redirect(Request.UrlReferrer.ToString());
        }
    }
}