﻿using ReaIVycle.app.Reporting;
using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using ReaIVycle.app.Controller;
using ReaIVycle.app.Reporting.RealCycleDataSetTableAdapters;
using System.Linq;

namespace ReaIVycle.app.View
{
    public partial class Report : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var user = (Model.User)Session["user"];

            if (user == null || (user != null && user.Role != "Admin"))
            {
                Response.RedirectToRoute("LoginRoute");
            }
            else
            {
                TransactionReport transactionReport = new TransactionReport();
                CrystalReportViewer1.ReportSource = transactionReport;

                var headers = TransactionController.GetTransactions();
                RealCycleDataSet dataset = GetData(headers);
                transactionReport.SetDataSource(dataset);
                transactionReport.Refresh();
            }
        }

        private RealCycleDataSet GetData(List<HeaderTransaction> headers)
        {
            RealCycleDataSet dataset = new RealCycleDataSet();
            var headerTable = dataset.HeaderTransaction;
            var detailTable = dataset.DetailTransaction;
            var productTable = dataset.Product;

            foreach (HeaderTransaction header in headers)
            {
                var transactionRow = headerTable.NewRow();
                transactionRow["ID"] = header.ID;
                transactionRow["Occurrence"] = header.Occurrence;
                transactionRow["Status"] = header.Status;
                headerTable.Rows.Add(transactionRow);

                foreach (DetailTransaction detail in header.DetailTransactions)
                {
                    var productRow = productTable.NewRow();
                    productRow["Name"] = detail.Product.Name;
                    productTable.Rows.Add(productRow);

                    var detailRow = detailTable.NewRow();
                    detailRow["HeaderID"] = detail.HeaderID;
                    detailRow["ProductID"] = detail.ProductID;
                    detailRow["Quantity"] = detail.Quantity;
                    detailRow["Price"] = detail.Price;
                    detailTable.Rows.Add(detailRow);
                }
            }

            return dataset;
        }
    }
}