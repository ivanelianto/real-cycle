﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="ReaIVycle.app.View.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
  Profile
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
  <link href="public/assets/css/profile.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
  <div class="container main-container">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-title">
            <h3>Profile</h3>
          </div>

          <div class="card-content">
            <div class="row">
              <div class="col-md-2 label">
                Name
              </div>

              <div class="col-md-10">
                <%= user.Name %>
              </div>
            </div>

            <div class="row">
              <div class="col-md-2 label">
                Email
              </div>

              <div class="col-md-10">
                <%= user.Email %>
              </div>
            </div>

            <div class="row">
              <div class="col-md-2 label">
                Birthday
              </div>

              <div class="col-md-10">
                <%= user.Birthday.Value.ToString("dddd, d MMMM yyyy") %>
              </div>
            </div>

            <div class="row">
              <div class="col-md-2 label">
                Gender
              </div>

              <div class="col-md-10">
                <%= user.Gender %>
              </div>
            </div>

            <div class="row">
              <div class="col-md-2 label">
                Address
              </div>

              <div class="col-md-10">
                <%= user.Address %>
              </div>
            </div>

            <div class="row">
              <div class="col-md-2 label">
                Phone
              </div>

              <div class="col-md-10">
                <%= user.Phone %>
              </div>
            </div>

            <div class="row">
              <div class="col-md-2 label">
                Role
              </div>

              <div class="col-md-10">
                <em><%= user.Role %></em>
              </div>
            </div>

            <div class="row">
              <div class="col-md-2 label">
                Password
              </div>

              <div class="col-md-10">
                  <asp:HyperLink NavigateUrl="/profile/change-password" 
                    Text="Change Password" 
                    class="btn btn-primary btn-change-password"
                    runat="server" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>