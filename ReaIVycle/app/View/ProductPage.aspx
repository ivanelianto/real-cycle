﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="ProductPage.aspx.cs" Inherits="ReaIVycle.app.View.ProductPage" %>
<%@Import Namespace="ReaIVycle.app.Util" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Products
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link href="../../../../public/assets/css/admin-product.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" runat="server">
        <% 
            if (Session["message"] != null)
            {
        %>
                <asp:Label
                    class="alert"
                    ID="txtMessage"
                    runat="server" />
        <% 
            }
        %>
        <div class="container main-container">
            <div class="row">
                <div class="col-md-5"></div>

                <div class="col-md-7 product-header">
                    <label>Filter by Category</label>

                    <span class="category-combobox-wrapper">
                        <asp:DropDownList
                            AutoPostBack="true"
                            OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"
                            ID="ddlCategory"
                            class="category-combobox"
                            runat="server" />
                    </span>

                    <asp:HyperLink runat="server"
                        class="btn btn-primary ml-3"
                        ID="btnAddNewProduct"
                        Visible="false"
                        NavigateUrl="/admin/manage-product/create">

                        <i class="fas fa-plus text-pale"></i> &nbsp; Add New Product

                    </asp:HyperLink>
                </div>
            </div>

            <div class="row">
                <asp:Repeater
                    OnItemCommand="productCards_ItemCommand"
                    OnItemDataBound="productCards_ItemDataBound"
                    OnItemCreated="productCards_ItemCreated"
                    ID="productCards"
                    runat="server">
                    <ItemTemplate>
                        <div class="col-md-3">
                            <asp:Panel
                                class="card df fdc product-card"
                                runat="server">

                                <div class="product-category-icon-wrapper">
                                    <asp:Image class="category-icon"
                                        ID="icoCategory"
                                        runat="server" />
                                </div>

                                <div class="product-image-wrapper">
                                    <asp:Image
                                        ID="productImage"
                                        runat="server" />
                                </div>

                                <asp:Panel class="admin-action-buttons"
                                    ID="adminActionButtonPanel"
                                    Visible="false"
                                    runat="server">
                                    <asp:HyperLink
                                        ID="btnEdit"
                                        class="action-button"
                                        runat="server">
                                        <i class="fas fa-edit text-pale"></i>
                                    </asp:HyperLink>

                                    <asp:LinkButton
                                        ID="btnDelete"
                                        class="action-button"
                                        CommandName="deleteProduct"
                                        runat="server">
                                        <i class="fas fa-trash text-pale"></i>
                                    </asp:LinkButton>
                                </asp:Panel>

                                <asp:HyperLink
                                    ID="btnProductDetail"
                                    OnClick="btnProductDetail_Click"
                                    runat="server">
                                    <asp:Label
                                        class="product-name"
                                        ID="lblProductName"
                                        runat="server" />

                                    <asp:Label
                                        class="product-price"
                                        ID="lblProductPrice"
                                        runat="server" />

                                    <asp:Label
                                        class="product-stock"
                                        ID="lblProductStock"
                                        Text="N available"
                                        runat="server" />
                                </asp:HyperLink>
                            </asp:Panel>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
