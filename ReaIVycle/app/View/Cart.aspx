﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="ReaIVycle.app.View.Cart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
  <link rel="stylesheet" href="../../public/assets/css/cart.css" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
  <div class="container main-container">
    <form method="POST" runat="server">
      <div class="row">
        <div class="col-md-8">
          <asp:Repeater OnItemDataBound="productCards_ItemDataBound" ID="cartItems" runat="server">
            <ItemTemplate>
              <div class="col-md-12 cart-row">
                <asp:Panel class="card df product-card" runat="server">

                  <asp:LinkButton 
                      ID="btnDelete"
                      class="delete-button"
                      CommandArgument='<%# Eval("HeaderID") + "," + Eval("ProductID") %>'
                      OnClick="btnDelete_Click"
                      runat="server">
                      <i class="fas fa-trash text-pale"></i>
                  </asp:LinkButton>

                  <div class="product-category-icon-wrapper">
                    <asp:Image class="category-icon" ID="icoCategory" runat="server" />
                  </div>

                  <div class="product-image-wrapper">
                    <asp:Image ID="productImage" runat="server" />
                  </div>

                  <asp:HyperLink class="product-in-cart-detail"
                    ID="btnProductDetail" OnClick="btnProductDetail_Click" runat="server">
                    <asp:Label class="product-name" ID="lblProductName" runat="server" />

                    <asp:Label class="product-price" ID="lblProductPrice" runat="server" />

                    <asp:Label class="product-stock" ID="lblQuantity" runat="server" />

                    <asp:Label class="subtotal" ID="lblSubtotal" runat="server" />
                  </asp:HyperLink></asp:Panel></div></ItemTemplate></asp:Repeater></div><div class="col-md-4">
          <div class="card df fdc">
            <div class="card-content cart-summary">
              Grandtotal <asp:Label ID="lblGrandtotal" CssClass="grandtotal" runat="server"/>
              <asp:Button 
                  ID="btnCheckout"
                  OnClick="btnCheckout_Click"
                  Text="Checkout"
                  Visible="false"
                  class="btn btn-primary"
                  runat="server"/>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>