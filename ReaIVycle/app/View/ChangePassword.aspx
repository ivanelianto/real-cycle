﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="ReaIVycle.app.View.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
  Change Password
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
  <div class="center-content-container">
    <div class="container">
      <%
          if (Session["error"] != null)
          {
      %>
              <p class="alert alert-danger">
                  <% 
                      Response.Write(Session["error"].ToString());
                      Session.Remove("error");
                  %>
              </p>
      <%
          }
          else if (Session["message"] != null)
          {
      %>
            <p class="alert alert-success">
              <% 
                  Response.Write(Session["message"].ToString());
                  Session.Remove("message");
              %>
            </p>
      <%
          }
      %>

      <div class="row">
        <div class="col-md-12">
          <form method="post" runat="server" class="password-wrapper">
            <div class="card">
              <div class="card-title">
                Change Password
              </div>

              <div class="card-content">
                <div class="row">
                  <div class="col-md-2 label">
                    Current Password
                  </div>

                  <div class="col-md-10">
                    <div class="input-wrapper">
                      <asp:TextBox placeholder="Current Password" TextMode="Password" ID="txtCurrentPassword"
                        runat="server" />
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2 label">
                    New Password
                  </div>

                  <div class="col-md-10">
                    <div class="input-wrapper">
                      <asp:TextBox placeholder="New Password" TextMode="Password" ID="txtNewPassword" runat="server" />
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2 label">
                    Confirm Password
                  </div>

                  <div class="col-md-10">
                    <div class="input-wrapper">
                      <asp:TextBox placeholder="Confirm New Password" TextMode="Password" ID="txtConfirmNewPassword"
                        runat="server" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="card-footer">
                <asp:Button class="btn btn-primary" ID="btnSave" Text="Save" OnClick="btnSave_Click" runat="server" />
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>