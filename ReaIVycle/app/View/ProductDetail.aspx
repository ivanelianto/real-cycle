﻿<%@ Page Title="" Language="C#" MasterPageFile="~/app/View/Shared/Layout.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="ReaIVycle.app.View.ProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    <%= product != null ? product.Name + " Detail" : "" %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="styles" runat="server">
    <link href="../../public/assets/css/admin-product-detail.css" rel="stylesheet" runat="server"/>
    <link href="../../public/assets/css/product-detail.css" rel="stylesheet" runat="server"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <form method="post" enctype="multipart/form-data" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <%
                        var errorMessage = Session["error"];

                        if (errorMessage != null)
                        {
                    %>
                            <label class="alert alert-danger">
                                <%= errorMessage %>
                            </label>
                    <%
                            Session.Remove("error");
                        }
                    %>
                    
                        
                    <div class="card">
                        <div class="product-image-wrapper">
                            <asp:Image
                                ID="productImage"
                                runat="server" />
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox
                                ReadOnly="true"
                                ID="txtName"
                                class="read-only-input"
                                placeholder="Name"
                                runat="server"/>
                            <label class="hint" for="name">Name</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox 
                                ReadOnly="true"
                                OnDataBound="ddlCategory_DataBound"
                                placeholder="Category"
                                class="read-only-input"
                                ID="txtCategory"
                                runat="server"/>
                            <label class="hint" for="txtCategory">Category</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox
                                class="read-only-input"
                                ReadOnly="true"
                                ID="txtPrice" 
                                runat="server"/>
                            <label class="hint" for="txtPrice">Price</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox
                                class="read-only-input"
                                ReadOnly="true"
                                ID="txtStock" 
                                runat="server"/>
                            <label class="hint" for="txtStock">Stock</label>
                        </div>

                        <div class="input-wrapper">
                            <asp:TextBox
                                class="read-only-input"
                                ReadOnly="true"
                                placeholder="Description"
                                TextMode="MultiLine"
                                ID="txtDescription" 
                                runat="server"/>
                            <label class="hint" for="txtDescription">Description</label>
                        </div>

                        <asp:Panel class="input-wrapper" 
                            ID="quantityPanel"
                            Visible="false"
                            runat="server">
                            <asp:TextBox
                                placeholder="Quantity"
                                ID="txtQuantity"
                                runat="server"/>
                            <label class="hint" for="txtQuantity">Quantity</label>
                        </asp:Panel>

                        <div class="action-wrapper">
                            <a href="/products" class="btn btn-secondary">Back</a>
                            
                            <button
                                class="btn btn-primary"
                                onserverclick="btnAddToCart_ServerClick"
                                id="btnAddToCart"
                                Visible="false"
                                runat="server">
                                <i class="fas fa-cart-plus"></i> &nbsp;
                                Add To Cart
                            </button>
                            
                            <button
                                class="btn btn-primary"
                                onserverclick="btnEdit_ServerClick"
                                id="btnEdit"
                                Visible="false"
                                runat="server">
                                <i class="fas fa-edit"></i> &nbsp;
                                Edit
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </form>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="scripts" runat="server">
</asp:Content>
