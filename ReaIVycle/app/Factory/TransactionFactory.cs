﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Model;

namespace ReaIVycle.app.Factory
{
    public static class TransactionFactory
    {
        public const string IN_CART_STATUS = "In Cart";
        public const string PENDING_STATUS = "Pending";
        public const string APPROVED_STATUS = "Approved";

        public static HeaderTransaction CreateHeader(HeaderTransactionDTO dto)
        {
            return new HeaderTransaction()
            {
                Occurrence = dto.Occurrence,
                UserID = dto.User.ID,
                Status = dto.Status
            };
        }

        public static HeaderTransaction CloneHeader(HeaderTransaction headerTransaction)
        {
            return new HeaderTransaction()
            {
                Occurrence = headerTransaction.Occurrence,
                UserID = headerTransaction.User.ID,
                Status = headerTransaction.Status,
            };
        }

        public static DetailTransaction CreateDetail(DetailTransactionDTO dto)
        {
            return new DetailTransaction()
            {
                HeaderID = dto.HeaderID,
                ProductID = dto.ProductID,
                Quantity = dto.Quantity,
                Price = dto.Price
            };
        }

        public static DetailTransaction CloneDetail(DetailTransaction detailTransaction)
        {
            return new DetailTransaction()
            {
                HeaderID = detailTransaction.HeaderID,
                Price = detailTransaction.Price,
                ProductID = detailTransaction.ProductID,
                Quantity = detailTransaction.Quantity,
            };
        }
    }
}