﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Model;
using System.IO;

namespace ReaIVycle.app.Factory
{
    public static class ProductFactory
    {
        public static Product Create(ProductDTO dto)
        {
            Product product = new Product()
            {
                CategoryID = int.Parse(dto.Category),
                Name = dto.Name,
                Price = double.Parse(dto.Price),
                Stock = int.Parse(dto.Stock),
                PicturePath = Path.GetFileName(dto.ImagePath),
                Description = dto.Description
            };

            if (!string.IsNullOrEmpty(dto.ID))
                product.ID = int.Parse(dto.ID);

            return product;
        }
    }
}