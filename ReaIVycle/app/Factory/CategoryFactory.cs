﻿using ReaIVycle.app.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Factory
{
    public static class CategoryFactory
    {
        public static Category Create(string name)
        {
            return new Category()
            {
                ID = 0,
                Name = name
            };
        }
    }
}