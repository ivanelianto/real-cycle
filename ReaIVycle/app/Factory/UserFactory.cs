﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Model;
using ReaIVycle.app.Util;
using System;

namespace ReaIVycle.app.Factory
{
    public static class UserFactory
    {
        private const string DEFAULT_ROLE = "Member";

        public static User Create(RegisterDTO dto)
        {
            return new User()
            {
                Name = dto.Name,
                Password = SecurityHelper.Hash(dto.Password),
                Email = dto.Email,
                Birthday = DateTime.Parse(dto.Birthday),
                Gender = dto.Gender,
                Phone = dto.Phone,
                Address = dto.Address,
                Role = DEFAULT_ROLE
            };
        }
    }
}