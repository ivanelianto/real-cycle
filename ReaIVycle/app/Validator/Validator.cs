﻿using System.Collections.Generic;
using System.Linq;

namespace ReaIVycle.app.Validator
{
    public abstract class Validator<DTO, Rule> 
        where DTO : class
        where Rule : class
    {
        public List<Rule> Rules { get; set; }
        public readonly DTO Data;

        public Validator(DTO dto)
        {
            this.Data = dto;
        }

        public abstract IEnumerable<string> Validate();
    }
}