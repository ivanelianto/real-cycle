﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Validator.Rule.ChangePassword;
using System.Collections.Generic;
using System.Linq;

namespace ReaIVycle.app.Validator
{
    public class ChangePasswordValidator : Validator<ChangePasswordDTO, ChangePasswordRule>
    {
        public ChangePasswordValidator(ChangePasswordDTO dto, string currentHashedPassword) : base(dto)
        {
            Rules = new List<ChangePasswordRule>()
            {
                new PasswordRule(currentHashedPassword)
            };
        }

        public override IEnumerable<string> Validate()
        {
            List<string> errorMessages = new List<string>();

            foreach (ChangePasswordRule rule in Rules)
            {
                if (!rule.IsValid(Data))
                    errorMessages.Add(rule.ErrorMessage);
            }

            return errorMessages;
        }
    }
}