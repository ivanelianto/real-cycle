﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Validator.Rule.Registration;
using System.Collections.Generic;

namespace ReaIVycle.app.Validator
{
    public class RegistrationValidator : Validator<RegisterDTO, RegistrationRule>
    {
        public RegistrationValidator(RegisterDTO dto) : base(dto)
        {
            Rules = new List<RegistrationRule>()
            {
                new NameRule(),
                new EmailRule(),
                new PasswordRule(),
                new GenderRule(),
                new AgeRule(),
                new PhoneRule(),
                new AddressRule()
            };
        }

        public override IEnumerable<string> Validate()
        {
            List<string> errorMessages = new List<string>();

            foreach (RegistrationRule rule in Rules)
            {
                if (!rule.IsValid(Data))
                    errorMessages.Add(rule.ErrorMessage);
            }

            return errorMessages;
        }

        //public RegistrationValidator(RegisterDTO dto)
        //{
        //    this.dto = dto;

        //    rules.AddRange(new List<IRegistrationRule>()
        //    {
        //        new EmailRule(),
        //        new PasswordRule(),
        //        new NameRule(),
        //        new GenderRule(),
        //        new AgeRule(),
        //        new PhoneRule(),
        //        new AddressRule()
        //    });
        //}
    }
}