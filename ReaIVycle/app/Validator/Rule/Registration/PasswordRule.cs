﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Validator.Rule.Shared;
using ReaIVycle.app.ValueObject;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public class PasswordRule : RegistrationRule
    {
        public override bool IsValid(RegisterDTO dto)
        {
            var rule = new GeneralPasswordRule();

            var password = new Password(null, dto.Password);

            var isValid = rule.IsValid(password, dto.ConfirmPassword);

            this.ErrorMessage = rule.ErrorMessage;

            return isValid;
        }
    }
}