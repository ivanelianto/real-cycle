﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public class GenderRule : RegistrationRule
    {
        public override bool IsValid(RegisterDTO dto)
        {
            if (dto.Gender == "")
            {
                ErrorMessage = "Gender must be selected.";
                return false;
            }

            return true;
        }
    }
}