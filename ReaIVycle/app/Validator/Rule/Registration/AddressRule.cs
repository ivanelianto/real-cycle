﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public class AddressRule : RegistrationRule
    {
        public override bool IsValid(RegisterDTO dto)
        {
            if (dto.Address == "")
            {
                ErrorMessage = "Address must be filled.";
                return false;
            }
            else if (!dto.Address.EndsWith("Street"))
            {
                ErrorMessage = "Address must ended with 'Street'";
                return false;
            }
            return true;
        }
    }
}