﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public class PhoneRule : RegistrationRule
    {
        private const int PHONE_LENGTH_LOWER_BOUND = 10;
        private const int PHONE_LENGTH_UPPER_BOUND = 12;

        public override bool IsValid(RegisterDTO dto)
        {
            if (!IsNumeric(dto.Phone))
            {
                ErrorMessage = "Phone number must be numeric.";
                return false;
            }
            else if (!IsValidPhoneLength(dto.Phone))
            {
                ErrorMessage = "Phone number must be between 10 and 12 characters.";
                return false;
            }

            return true;
        }

        private bool IsValidPhoneLength(string phone)
        {
            return phone.Length <= PHONE_LENGTH_UPPER_BOUND &&
                phone.Length >= PHONE_LENGTH_LOWER_BOUND;
        }
    }
}