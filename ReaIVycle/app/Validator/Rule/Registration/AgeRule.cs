﻿using System;
using ReaIVycle.app.DTO;
using ReaIVycle.app.Model;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public class AgeRule : RegistrationRule
    {
        private const int YOUGSET_VALID_AGE = 17;
        private const int PHONE_LENGTH_LOWER_BOUND = 10;
        private const int PHONE_LENGTH_UPPER_BOUND = 12;

        public override bool IsValid(RegisterDTO dto)
        {
            if (dto.Birthday == "")
            {
                ErrorMessage = "Birthday must be filled.";
                return false;
            }

            Age age = new Age(dto.Birthday, DateTime.Now);

            if (age.Years < YOUGSET_VALID_AGE)
            {
                ErrorMessage = "You must be at least 17 years old to continue.";
                return false;
            }

            return true;
        }
    }
}