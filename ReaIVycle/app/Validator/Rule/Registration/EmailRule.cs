﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Handler;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public class EmailRule : RegistrationRule
    {
        public override bool IsValid(RegisterDTO dto)
        {
            if (dto.Email == "")
            {
                ErrorMessage = "Email must be filled.";
                return false;
            }
            else if (UserHandler.FindUserByEmail(dto.Email) != null)
            {
                ErrorMessage = "Email has been taken.";
                return false;
            }

            return true;
        }
    }
}