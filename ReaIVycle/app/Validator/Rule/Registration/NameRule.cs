﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public class NameRule : RegistrationRule
    {
        public override bool IsValid(RegisterDTO dto)
        {
            if (string.IsNullOrEmpty(dto.Name))
            {
                ErrorMessage = "Name must be filled.";
                return false;
            }

            return true;
        }
    }
}