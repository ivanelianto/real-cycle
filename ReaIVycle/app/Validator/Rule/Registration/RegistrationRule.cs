﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Registration
{
    public abstract class RegistrationRule : BaseRule
    {
        public abstract bool IsValid(RegisterDTO dto);
    }
}