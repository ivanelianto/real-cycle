﻿using ReaIVycle.app.DTO;
using System.IO;

namespace ReaIVycle.app.Validator.Rule.Product
{
    public class ImageRule : ProductRule
    {
        public override bool IsValid(ProductDTO dto)
        {
            if (string.IsNullOrEmpty(dto.ImagePath))
            {
                ErrorMessage = "Product Image Must Be Selected.";
                return false;
            }
            else if (IsValidImageFileExtension(dto))
            {
                ErrorMessage = "Picture File Must Be .jpg or .png";
                return false;
            }

            return true;
        }

        private static bool IsValidImageFileExtension(ProductDTO dto)
        {
            return Path.GetExtension(dto.ImagePath) != ".jpg" &&
                Path.GetExtension(dto.ImagePath) != ".png";
        }
    }
}