﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Util;

namespace ReaIVycle.app.Validator.Rule.Product
{
    public class StockRule : ProductRule
    {
        private const int MINIMUM_STOCK = 1;

        public override bool IsValid(ProductDTO dto)
        {
            int result = Constants.ERROR_CODE;

            if (!int.TryParse(dto.Stock, out result))
            {
                ErrorMessage = "Stock Must Be Numeric.";
                return false;
            }
            else if (result < MINIMUM_STOCK)
            {
                ErrorMessage = "Stock Must Be Greater Than 0.";
                return false;
            }

            return true;
        }
    }
}