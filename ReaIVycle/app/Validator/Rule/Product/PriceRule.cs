﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Util;

namespace ReaIVycle.app.Validator.Rule.Product
{
    public class PriceRule : ProductRule
    {
        private const int MINIMUM_PRICE = 1;

        public override bool IsValid(ProductDTO dto)
        {
            int result = Constants.ERROR_CODE;

            if (!int.TryParse(dto.Price, out result))
            {
                ErrorMessage = "Product Price Must Be Numeric.";
                return false;
            }
            else if (result < MINIMUM_PRICE)
            {
                ErrorMessage = "Product Price Must Be Greater Than 0.";
                return false;
            }

            return true;
        }
    }
}