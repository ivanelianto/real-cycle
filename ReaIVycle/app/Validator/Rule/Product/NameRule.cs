﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Product
{
    public class NameRule : ProductRule
    {
        public override bool IsValid(ProductDTO dto)
        {
            if (string.IsNullOrEmpty(dto.Name))
            {
                ErrorMessage = "Product Name Must Be Filled.";
                return false;
            }

            return true;
        }
    }
}