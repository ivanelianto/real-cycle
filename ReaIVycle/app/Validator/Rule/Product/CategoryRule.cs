﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Product
{
    public class CategoryRule : ProductRule
    {
        public override bool IsValid(ProductDTO dto)
        {
            if (dto.Category == "0")
            {
                ErrorMessage = "Category Must Be Selected.";
                return false;
            }

            return true;
        }
    }
}