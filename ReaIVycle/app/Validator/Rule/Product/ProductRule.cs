﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Product
{
    public abstract class ProductRule : BaseRule
    {
        public abstract bool IsValid(ProductDTO dto);
    }
}