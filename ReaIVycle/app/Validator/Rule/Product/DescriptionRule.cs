﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.Product
{
    public class DescriptionRule : ProductRule
    {
        public override bool IsValid(ProductDTO dto)
        {
            string[] words = dto.Description.Split(' ');

            if (words.Length < 7)
            {
                ErrorMessage = "Product Description Must Be At Least 7 Words.";
                return false;
            }

            return true;
        }
    }
}