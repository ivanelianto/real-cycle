﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.ValueObject;
using ReaIVycle.app.Util;

namespace ReaIVycle.app.Validator.Rule.Shared
{
    public class GeneralPasswordRule : BaseRule
    {
        private const int MINIMUM_LENGTH = 8;

        public bool IsValid(Password inputtedPassword, string passwordConfirmation)
        {
            var password = inputtedPassword.Recent;

            return !IsEmptyPassword(password) && 
                IsAlphanumericPassword(password) &&
                IsEnoughLength(password) &&
                IsConfirmed(password, passwordConfirmation);
        }

        private bool IsEmptyPassword(string password)
        {
            if (password == "")
            {
                ErrorMessage = "Password must be filled.";
                return true;
            }

            return false;
        }

        private bool IsAlphanumericPassword(string password)
        {
            if (!IsAlphanumeric(password))
            {
                ErrorMessage = "Password must be alphanumeric.";
                return false;
            }

            return true;
        }

        private bool IsEnoughLength(string password)
        {
            if (password.Length < MINIMUM_LENGTH)
            {
                ErrorMessage = "Password must be at least 8 characters.";
                return false;
            }

            return true;
        }

        private bool IsConfirmed(string password, string passwordConfirmation)
        {
            if (password != passwordConfirmation)
            {
                ErrorMessage = "Password confirmation doesn\'t match.";
                return false;
            }

            return true;
        }
    }
}