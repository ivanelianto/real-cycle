﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.AddToCart
{
    public abstract class AddToCartRule : BaseRule
    {
        public abstract bool IsValid(AddToCartDTO dto);
    }
}