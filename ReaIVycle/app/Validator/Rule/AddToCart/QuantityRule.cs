﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Util;

namespace ReaIVycle.app.Validator.Rule.AddToCart
{
    public class QuantityRule : AddToCartRule
    {
        private const int MINIMUM_QUANTITY = 1;

        public override bool IsValid(AddToCartDTO dto)
        {
            int quantity = Constants.ERROR_CODE;

            if (!int.TryParse(dto.Quantity, out quantity))
            {
                ErrorMessage = "Quantity Must Be Numeric.";
                return false;
            }
            else if (!IsMoreThanZero(quantity))
            {
                ErrorMessage = "Quantity Must Be Greater Than Zero.";
                return false;
            }
            else if (!IsLessThanEqualWithStock(quantity, int.Parse(dto.Stock)))
            {
                ErrorMessage = "Quantity Must Be Less Than Or Equals With Stock.";
                return false;
            }

            return true;
        }

        private bool IsMoreThanZero(int quantity)
        {
            return quantity >= MINIMUM_QUANTITY;
        }

        private bool IsLessThanEqualWithStock(int quantity, int stock)
        {
            return quantity <= stock;
        }
    }
}