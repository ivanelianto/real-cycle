﻿using ReaIVycle.app.DTO;

namespace ReaIVycle.app.Validator.Rule.ChangePassword
{
    public abstract class ChangePasswordRule : BaseRule
    {
        public abstract bool IsValid(ChangePasswordDTO dto);
    }
}