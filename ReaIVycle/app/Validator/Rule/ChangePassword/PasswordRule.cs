﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Util;
using ReaIVycle.app.Validator.Rule.Shared;

namespace ReaIVycle.app.Validator.Rule.ChangePassword
{
    public class PasswordRule : ChangePasswordRule
    {
        private string currentHashedPassword;

        public PasswordRule(string currentHashedPassword)
        {
            this.currentHashedPassword = currentHashedPassword;
        }

        public override bool IsValid(ChangePasswordDTO dto)
        {
            var rule = new GeneralPasswordRule();

            var isValidPassword = rule.IsValid(dto.Password, dto.ConfirmPassword);

            if (!isValidPassword)
                this.ErrorMessage = rule.ErrorMessage;
            
            return IsValidOldPassword(dto.Password.Old) && 
                isValidPassword;
        }

        private bool IsValidOldPassword(string oldPassword)
        {
            if (currentHashedPassword != SecurityHelper.Hash(oldPassword))
            {
                ErrorMessage = "Invalid old password.";
                return false;
            }

            return true;
        }
    }
}