﻿using System;

namespace ReaIVycle.app.Validator.Rule
{
    public abstract class BaseRule
    {
        /**
         * Untuk Menampung Pesan Kegagalan
         */
        public string ErrorMessage { get; set; }

        /**
         * Untuk Cek Apakah Semua Angka
         */
        protected bool IsNumeric(string text)
        {
            foreach (char c in text)
                if (!char.IsDigit(c))
                    return false;
            return true;
        }

        /**
         * Untuk Cek Apakah Ada Angka dan Huruf
         */
        protected bool IsAlphanumeric(string text)
        {
            bool containsDigit = false,
                containsLetter = false;

            foreach (char c in text)
            {
                if (char.IsDigit(c))
                    containsDigit = true;
                else if (char.IsLetter(c))
                    containsLetter = true;
            }

            return containsDigit && containsLetter;
        }
    }
}