﻿using System;
using System.Collections.Generic;
using ReaIVycle.app.DTO;
using ReaIVycle.app.Validator.Rule.AddToCart;

namespace ReaIVycle.app.Validator
{
    public class AddToCartValidator : Validator<AddToCartDTO, AddToCartRule>
    {
        public AddToCartValidator(
            AddToCartDTO dto) : base(dto)
        {
            Rules = new List<AddToCartRule>()
            {
                new QuantityRule()
            };
        }

        public override IEnumerable<string> Validate()
        {
            List<string> errorMessages = new List<string>();

            foreach (AddToCartRule rule in Rules)
                if (!rule.IsValid(Data))
                    errorMessages.Add(rule.ErrorMessage);

            return errorMessages;
        }
    }
}