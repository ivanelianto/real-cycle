﻿using System.Collections.Generic;
using ReaIVycle.app.DTO;
using ReaIVycle.app.Validator.Rule.Product;

namespace ReaIVycle.app.Validator
{
    public class ProductValidator : Validator<ProductDTO, ProductRule>
    {
        public ProductValidator(ProductDTO dto) : base(dto)
        {
            Rules = new List<ProductRule>()
            {
                new NameRule(),
                new CategoryRule(),
                new PriceRule(),
                new StockRule(),
                new DescriptionRule(),
                new ImageRule()
            };
        }

        public override IEnumerable<string> Validate()
        {
            List<string> errorMessages = new List<string>();

            foreach (ProductRule rule in Rules)
            {
                if (!rule.IsValid(Data))
                    errorMessages.Add(rule.ErrorMessage);
            }

            return errorMessages;
        }
    }
}