﻿using ReaIVycle.app.DTO;
using ReaIVycle.app.Model;
using ReaIVycle.app.View.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReaIVycle.app.Validator
{
    public class RegisterValidator : Validator
    {
        private const int YOUGSET_VALID_AGE = 17;
        private const int PHONE_LENGTH_LOWER_BOUND = 10;
        private const int PHONE_LENGTH_UPPER_BOUND = 12;

        RegisterDTO dto;

        public RegisterValidator(
               RegisterDTO dto
            )
        {
            this.dto = dto;
        }

        public bool IsValidInputs()
        {
            return IsValidUsername() &&
                IsValidPassword() &&
                IsSameWithPassword() &&
                IsValidEmail() &&
                IsValidName() &&
                IsGenderSelected() &&
                IsMatureEnough() &&
                IsValidPhone() &&
                IsValidAddress();
        }

        private bool IsValidUsername()
        {
            if (dto.Username == "")
            {
                ErrorMessage = "Username must be filled.";
                return false;
            }
            return true;
        }

        private bool IsValidName()
        {
            if (dto.Name == "")
            {
                ErrorMessage = "Name must be filled.";
                return false;
            }
            return true;
        }

        private bool IsValidPassword()
        {
            if (dto.Password == "")
            {
                ErrorMessage = "Password must be filled.";
                return false;
            }
            else if (!IsAlphanumeric(dto.Password))
            {
                ErrorMessage = "Password must be alphanumeric.";
                return false;
            }

            else if (dto.Password.Length < 8)
            {
                ErrorMessage = "Password must be at least 8 characters.";
                return false;
            }
            return true;
        }

        private bool IsAlphanumeric(String text)
        {
            bool containsDigit = false,
                containsLetter = false;

            foreach (char c in text)
            {
                if (Char.IsDigit(c))
                {
                    containsDigit = true;
                }
                else if (Char.IsLetter(c))
                {
                    containsLetter = true;
                }
            }

            return containsDigit && containsLetter;
        }

        private bool IsSameWithPassword()
        {
            if (dto.Password != dto.ConfirmPassword)
            {
                ErrorMessage = "Password confirmation doesn't match with password.";
                return false;
            }

            return true;
        }

        private bool IsValidEmail()
        {
            if (dto.Email == "")
            {
                ErrorMessage = "Email must be filled.";
                return false;
            }

            return true;
        }

        private bool IsGenderSelected()
        {
            if (dto.Gender == "")
            {
                ErrorMessage = "Gender must be selected.";
                return false;
            }

            return true;
        }

        private bool IsMatureEnough()
        {
            if (dto.Birthday == "")
            {
                ErrorMessage = "Birthday must be filled.";
                return false;
            }

            DateTime birthday = DateTime.Parse(dto.Birthday);
            DateTime now = DateTime.Now;
            Age age = new Age(birthday, now);

            if (age.Years < YOUGSET_VALID_AGE)
            {
                ErrorMessage = "You must be at least 17 years old to continue.";
                return false;
            }

            return true;
        }

        private bool IsValidPhone()
        {
            if (!IsNumeric(dto.Phone))
            {
                ErrorMessage = "Phone number must be numeric.";
                return false;
            }
            else if (!IsValidPhoneLength())
            {
                ErrorMessage = "Phone number must be between 10 and 12 characters.";
                return false;
            }
            return true;
        }

        private bool IsNumeric(String text)
        {
            foreach (char c in text)
                if (!char.IsDigit(c))
                    return false;
            return true;
        }

        private bool IsValidPhoneLength()
        {
            return dto.Phone.Length <= PHONE_LENGTH_UPPER_BOUND &&
                dto.Phone.Length >= PHONE_LENGTH_LOWER_BOUND;
        }

        private bool IsValidAddress()
        {
            if (dto.Address == "")
            {
                ErrorMessage = "Address must be filled.";
                return false;
            }
            else if (!dto.Address.EndsWith("Street"))
            {
                ErrorMessage = "Address must ended with 'Street'";
                return false;
            }
            return true;
        }
    }
}