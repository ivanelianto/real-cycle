﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace ReaIVycle.app.Util
{
    public class ScriptHelper
    {
        public static void Alert(Page page, String text)
        {
            ScriptManager.RegisterClientScriptBlock(
                page, 
                page.GetType(), 
                "script", 
                "alert(1)", 
                true);
        }
    }
}