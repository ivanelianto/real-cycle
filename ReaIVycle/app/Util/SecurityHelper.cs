﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ReaIVycle.app.Util
{
    public class SecurityHelper
    {
        public static string Hash(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            HashAlgorithm algorithm = HashAlgorithm.Create("SHA256");

            return Convert.ToBase64String(algorithm.ComputeHash(bytes));
        }
    }
}