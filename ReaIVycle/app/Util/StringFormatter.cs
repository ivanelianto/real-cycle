﻿namespace ReaIVycle.app.Util
{
    public static class StringFormatter
    {
        public static string GetCurrenciesFormat(double value)
        {
            return value.ToString("C", new System.Globalization.CultureInfo("id-ID"));
        }
    }
}