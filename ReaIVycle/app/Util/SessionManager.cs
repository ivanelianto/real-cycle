﻿using ReaIVycle.app.Model;

namespace ReaIVycle.app.Util
{
    public class SessionManager : System.Web.UI.Page
    {
        public static SessionManager instance;

        private SessionManager() { }

        public static SessionManager GetInstance()
        {
            if (instance == null)
                instance = new SessionManager();

            return instance;
        }

        public User GetCurrentUser()
        {
            if (Session["user"] != null)
                return (User)Session["user"];

            return null;
        }

        public void SetErrorMessage(string message)
        {
            Session["error"] = message;
        }
    }
}