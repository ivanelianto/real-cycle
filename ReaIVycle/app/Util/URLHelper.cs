﻿using System.Linq;
using System.Web.UI;

namespace ReaIVycle.app.Util
{
    public class URLHelper
    {
        public static string GetIDForDetailPage(Page page)
        {
            return page.Request.Url.Segments.Last();
        }

        public static string GetIDForEditPage(Page page)
        { 
            var segments = page.Request.Url.Segments;
            return segments.ElementAt(segments.Length - 2).Split('/')[0];
        }
    }
}