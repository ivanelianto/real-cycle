﻿namespace ReaIVycle.app.ValueObject
{
    public class Password
    {
        public string Old { get; }
        public string Recent { get; }

        public Password(string old, string recent)
        {
            this.Old = old;
            this.Recent = recent;
        }
    }
}